#include "stdafx.h"
#include "analyse_body.h"
#include "byteconversions.h"
#include "logging.h"
#include "helper.h"
#include "enums.h"

int analyseBodyData(byte* bodyData, int bodyLength, GameData* pGameData)
{
    int offset = 0;
    byte* d = bodyData;
    while(offset < bodyLength)   //if the offset becomes less than before, something is wrong, stop scanning the data
    {
//        printf("offset: %d, bodyLength: %d\n", offset, bodyLength);
        int opeType = convertBytesToInt(&d[offset]);
        switch (opeType)
        {
//            case 2:
//                log("type 2\n");
//                offset = stepOverOpeType2(d, offset + 4);
//                break;
            case 4:
//                log("type 4\n");
                offset = stepOverOpeType4(d, offset + 4);
                break;
            case 1:
//                log("type 1\n");
                offset = analyseOpeType1(bodyData, offset + 4, pGameData);
                break;
            default:
                offset++;   //none found, moves one byte forward
        }
    }
    return 0;
}

int stepOverOpeType2(byte* bodyData, int startOffset)
{
//    printf("stepOverOpeType2\n");
    byte* d = bodyData;
    int offset = startOffset;
    offset += 4;
    int unknown = convertBytesToInt(&d[offset]);
    offset += 4;    //unknown
    if(0 == unknown)
    {
        offset += 28;
    }
    offset += 12;   //players view x,y and player index
    return offset;
}

int stepOverOpeType4(byte* bodyData, int startOffset)
{
//    printf("stepOverOpeType4\n");
    byte* d = bodyData;
    int offset = startOffset;
    int command = convertBytesToInt(&d[offset]);
    offset += 4;
    if(0x1f4 == command)    //game start
    {
        offset += 20;
    }
    else if(0xffffffff == (unsigned)command)  //chat. Casted to unsigned to avoid warning.
    {
        int chatLen = convertBytesToInt(&d[offset]);
        offset += 4;

        //uncomment the following block to print chat
        char* buf = (char*)malloc(chatLen * sizeof(char));
        convertBytesToString(&d[offset], buf, chatLen);
        log_(MODERATE, "%s\n", buf);
        free(buf);

        offset += chatLen;
    }
    return offset;
}

int analyseOpeType1(byte* bodyData, int startOffset, GameData* pGameData)
{
    byte* d = bodyData;
    int offset = startOffset;
//    printf("offset: %d\n", offset);
    int length = convertBytesToInt(&d[offset]);
    offset+=4;
//    printf("length: %d\n", length);
    if(4 == length) //this can contain a resign command.
    {
        byte commandType = d[offset];
        if(0x0b == commandType) //resign!
        {
            int playerNumber = d[offset + 2];
            pGameData->players[playerNumber - 1].resigned = 1;
            log_(MODERATE, "player %d resigned\n", playerNumber);
        }
    }
    if(4 == length|| 12 == length)
    {
        offset += length;
    }
    else
    {
        offset++;   //stupid length found. Just moves one byte forward
    }
    return offset;
}

void analyseBodyData2(byte* bodyData, int bodyLength, GameData* pGameData)
{
    logFunctionEntered("analyseBodyData2()");

    int offset = 0;
    byte* d = bodyData;

    while(offset < bodyLength - 4)
    {
        if(0x0b == d[offset]&&
           (0x04 == convertBytesToInt(&d[offset - 4]) || 0x10 == convertBytesToInt(&d[offset - 4])) &&   //length can be 4 or 16
           1 <= d[offset + 1] && d[offset + 1] <= MAX_PLAYERS &&
           1 <= d[offset + 2] && d[offset + 2] <= MAX_PLAYERS)
        {
            printByteArray(&d[offset - 4], 8, VERBOSE);

            //resign command found
            byte coopIndex = d[offset + 1];
            byte playerNumber = d[offset + 2];
            log_(MODERATE, "resigned: player number: %d, coop index: %d\n", playerNumber, coopIndex);
            int i = 0;
            for (i = 0; i < MAX_PLAYERS; i++)
            {
                Player* p = &pGameData->players[i];
                if(p->number == playerNumber && p->coopIndex == coopIndex)
                    p->resigned = 1;
            }
        }
        offset++;
    }
    logFunctionReturnsVoid("analyseBodyData2()");
}
