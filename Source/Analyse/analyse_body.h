#ifndef BODYANALYSING_H
#define BODYANALYSING_H

#include <windows.h>
#include "GameDataStruct/gamedata.h"

/*! \file analyse_body.h Functions to analyse the body of the recorded game. */

/*!
 * \brief analyseBodyData Analyses the body data for commands described in the mgx reference.
 * This function is not used currently, because there are a lot more commands that are documented in the reference
 * and it wasn't robust enough. It is still able to extract chat messages.
 * \param bodyData Pointer to the binary data of the rec body.
 * \param bodyLength Length of the body in bytes.
 * \param pGameData Pointer to the \see GameData structure. The extracted data will be saved to it.
 * \return 0 on a successful analysis. Does not return error, not implemented.
 */
int analyseBodyData(byte* bodyData, int bodyLength, GameData* pGameData);

/*!
 * \brief stepOverOpeType2 Steps over the opeType2 blocks in the data.
 * Calculates the length of the block and returns the index where the block finishes. We are not interested in this block.
 * \param bodyData Pointer to the binary data of the rec body.
 * \param startOffset The index in the bodyData where the opeType2 block starts.
 * \return The index after the block, so the search can continue.
 */
int stepOverOpeType2(byte* bodyData, int startOffset);

/*!
 * \brief stepOverOpeType4 Steps over the opeType4 blocks in the data.
 * Calculates the length of the block and returns the index where the block finishes. We are not interested in this block.
 * \param bodyData Pointer to the binary data of the rec body.
 * \param startOffset The index in the bodyData where the opeType4 block starts.
 * \return The index after the block, so the search can continue.
 */
int stepOverOpeType4(byte* bodyData, int startOffset);

/*!
 * \brief analyseOpeType1 Checks if this opeType1 contains a resign command.
 * \param bodyData Pointer to the binary data of the rec body.
 * \param startOffset The index in the bodyData where the opeType4 block starts.
 * \param pGameData Pointer to the \see GameData structure. The extracted data will be saved to it.
 * \return The index after the block, so the search can continue.
 */
int analyseOpeType1(byte* bodyData, int startOffset, GameData* pGameData);

/*!
 * \brief analyseBodyData2 Scans through the body data searching for resign command patterns.
 * Does not try to step by the Ope blocks as \see analyseBodyData, because that did not find all resign commands and sometimes crashed.
 * \param bodyData Pointer to the binary data of the rec body.
 * \param bodyLength Length of the body in bytes.
 * \param pGameData pGameData Pointer to the \see GameData structure. The extracted data will be saved to it.
 */
void analyseBodyData2(byte* bodyData, int bodyLength, GameData* pGameData);

#endif // BODYANALYSING_H
