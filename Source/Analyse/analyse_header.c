#include "stdafx.h"
#include "analyse_header.h"
#include "enums.h"
#include "helper.h"
#include "byteconversions.h"
#include "lookups.h"
#include "locatefunctions.h"
#include "zlibfunctions.h"

int getHeaderData(byte* compressedData, int compressedDataSize, GameData* pGameData)
{
    logFunctionEntered("getHeaderData()");

    byte* decompressedData = NULL;
    int decompressedDataSize = 0;

    int retVal = zlibDecompress(compressedData, compressedDataSize, &decompressedData, &decompressedDataSize);
    if(retVal != Z_OK)
    {
        log_(BASIC, "<- zlibDecompress() failed and returned %d, getHeaderData() returns %d\n", retVal, retVal);
        return retVal;
    }

    if(!isAcceptedVersion((const char*)decompressedData))   //cast to avoid warning
    {
        logFunctionReturnsInt("getHeaderData()", BAD_DATA, "Not accepted version.", BASIC);
        return BAD_DATA;
    }
    retVal = analyseHeaderData(decompressedData, decompressedDataSize, pGameData);
    if(retVal != OK)
    {
        logFunctionReturnsInt("getHeaderData()", retVal, "", BASIC);
        return retVal;
    }

    free(decompressedData);
    log_(VERBOSE, "Decompressed data freed.\n");
    logFunctionReturnsInt("getHeaderData()", OK, "", VERBOSE);
    return OK;
}

int analyseHeaderData(byte* decompressedData, int decompressedDataSize, GameData* pGameData)
{
    logFunctionEntered("analyseHeaderData()");
    RetVal retVal;

    if(NULL == pGameData)
    {
        logFunctionReturnsInt("analyseHeaderData()", NULL_PARAMETER, "", BASIC);
        return NULL_PARAMETER;
    }

    int offset = 0;
    byte* d = decompressedData; //for the sake of a shorter name

    offset = 12;    //constant bytes at the beginning
    byte AIIncluded = d[offset];
    offset += 4;    //ai_included
    int aiInfoLength = 0 == AIIncluded ? 0 : countAIInfoLength(d, offset);
    offset += aiInfoLength;

    offset += 4;    //constant bytes, unknown int
    offset += 8;    //gamespeed1 + zero int
    offset += 29 + 4;   //4+4+4+17 gamespeed2 + unknown fields + POV player + 4, I dont know why, probably the data schema is not correct
    pGameData->povPlayer = convertBytesToShort(&d[offset]);
    log_(MODERATE, "\nPOV player found: %d\n", pGameData->povPlayer);
    if(pGameData->povPlayer > MAX_PLAYERS)
    {
        logFunctionReturnsInt("analyseHeaderData()", BAD_DATA, "POV player is greater than MAX_PLAYERS. Bad data.", BASIC);
        return BAD_DATA;
    }
    offset += 2;    //POV
    pGameData->numPlayersWithoutGaia = d[offset] - 1;    //+1 is Gaia
    log_(MODERATE, "\nNumber of players found: %d\n\n", pGameData->numPlayersWithoutGaia);
    if(pGameData->numPlayersWithoutGaia > MAX_PLAYERS)
    {
        logFunctionReturnsInt("analyseHeaderData()", BAD_DATA, "Number of players is greater than MAX_PLAYERS. Bad data.", BASIC);
        return BAD_DATA;
    }

    offset += 1;    //numPlayers

    byte pattern[] = {0x98, 0x9e, 0x00, 0x00, 0x02, 0x0b};
    offset = seekPattern(d, pattern, 6, 0, decompressedDataSize);
    if(offset < 0)
    {
        logFunctionReturnsInt("analyseHeaderData()", BAD_DATA, "Pattern not found, bad data.", BASIC);
        return BAD_DATA;  //pattern not found, incorrect data
    }
    analysePlayerInfo(d, decompressedDataSize, offset, pGameData);

    int scenarioHeaderOffset = locateScenarioHeader(d, decompressedDataSize, offset);
    offset = locateInstructions(d, scenarioHeaderOffset);
    offset += 24;
    unsigned short instructionsLength = convertBytesToShort(&d[offset]);
    offset += 2;
    if(instructionsLength > 0)
    {
        //saves instructions in case we dont have a valid map ID.
        char* buf = (char*)malloc(sizeof(char) * instructionsLength);
        convertBytesToString(&d[offset], buf, instructionsLength);
        pGameData->instructions = buf;
        offset += instructionsLength;

        log_(VERBOSE, "Instructions found: \n%s\n", pGameData->instructions);
    }

    //reading map id
    offset = locateGameSettings(d, decompressedDataSize, offset);
    offset += 12;
    pGameData->mapId = convertBytesToInt(&d[offset]);
    log_(MODERATE, "\nMap ID found: %d: %s\n\n", pGameData->mapId, getMapNameById(pGameData->mapId));

    //checking player info for coops
    offset += 12;
    retVal = analyseGameSettingsForCoops(d, offset, pGameData);
    if(retVal != OK)
    {
        logFunctionReturnsInt("analyseHeaderData()", retVal, "", BASIC);
        return retVal;
    }

    //locating and reading other data: teams and game type (random map, scenario, etc...)
    offset = locateOtherData(decompressedData, decompressedDataSize);
    int i = 0;
    log_(VERBOSE, "Teams in the data:\n");
    printByteArray(&d[offset], 8, VERBOSE);
    for (i = 0; i < pGameData->numPlayersWithoutGaia; i++)
    {
        //GAIA does not have team. -1, because 1 means no team in the data. Here we store 0 for indicating no team
        pGameData->players[i].team = d[offset + i] - 1;
        log_(VERBOSE, "team: %d\n", pGameData->players[i].team);
    }
    pGameData->gameType = d[offset + 25];

    log_(MODERATE, "Game data after picking up the teams:\n");
    printGameData(pGameData, MODERATE);

    logFunctionReturnsInt("analyseHeaderData()", OK, "", VERBOSE);
    return OK;
}

void analysePlayerInfo(byte* decompressedData, int decompressedDataSize, int startOffset, GameData* pGameData)
{
    logFunctionEntered("analysePlayerInfo()");

    byte* d = decompressedData;
    int offset = startOffset;
    int numPlayers = pGameData->numPlayersWithoutGaia;
    int numPlayersFound = 0, found = 0;

    int i = 0;

    while(numPlayersFound <= numPlayers && offset < decompressedDataSize)
    {
        found = 1;
        short length = convertBytesToShort(&d[offset]); //trying this two bytes as length
        if(length <= 0)
        {
            offset++;
            continue;
        }

        for (i = 2; i < length + 1 && 1 == found; i++)
        {
            if(0 == d[offset + i])  //zero found before the length, it cannot be a string
            {
                found = 0;
            }
        }
        if(1 == found && d[offset + length + 1] != 0)  //this should be the trailing zero...
        {
            found = 0;  //...but it is not, string not found
        }

        //05 00 xx xx xx xx 00 xx C6    //example pattern   05 00 is the length, xx are the chars in the string, 00 is the string trailing zero
        //C6 is 198 in decimal
        int c6Offset = length + 3;
        if(1 == found && 198 == d[offset + c6Offset])  //if 198 is found, we found a player name
        {
            if(numPlayersFound > 0) //we are not interested in GAIA
            {
                Player* pPlayer = &pGameData->players[numPlayersFound - 1];  //when one player is already found (Gai), then we put this real player to index 0.

                char* buf = (char*)malloc(sizeof(char) * (length)); //the length already contains the trailing zero
                pPlayer->name = convertBytesToString(&d[offset + 2], buf, length);
                log_(MODERATE, "Player found: %s\n", buf);

                byte civ = d[offset + c6Offset + 5 + 100 + 80 + 200 + 412 + 18];  //815 bytes after 198
                pPlayer->civ = civ;

                log_(MODERATE, "Civ found: %d: %s\n", civ, civs[civ]);

                if(numPlayersFound == pGameData->povPlayer)
                    pPlayer->pov = 1;

                //filling up the diplomacy arrays. It can be used if the teams data is invalid for some reason (in coop games for example)
                int diplomacyOffset = offset - 5 - 36 - (numPlayers); //numPlayers byte (diplomacy from) and 9 int (diplomacy to)
                log_(VERBOSE, "Diplomacy data: \n");
                printByteArray(&d[diplomacyOffset], numPlayers + 1 + 36, VERBOSE);

                for (i = 0; i < numPlayers; i++)    //gaia has an entry too, but we don't save it. It can be 8 max
                {
                    pPlayer->diplomacyFrom[i] = d[diplomacyOffset++];
                }
                diplomacyOffset += 4;   //jumps over the Gaia diplomacy
                for (i = 0; i < 8; i++)    //this is always 9, the ones not used are filled up with -1 in the data. Starts from 1, we don't save Gaia
                {
                    pPlayer->diplomacyTo[i] = convertBytesToInt(&d[diplomacyOffset]);
                    diplomacyOffset += 4;
                }
            }
            numPlayersFound++;
            offset += c6Offset;
        }
        else
        {
            offset++;
        }
    }
    logFunctionReturnsVoid("analysePlayerInfo()");
}

int analyseGameSettingsForCoops(byte* decompressedData, int startOffset, GameData* pGameData)
{
    logFunctionEntered("analyseGameSettingsForCoops()");

    byte* d = decompressedData;
    int offset = startOffset;

    log_(VERBOSE, "Game data before coop analysis:\n");
    printGameData(pGameData, VERBOSE);

    int i = 0;
    for (i = 0; i < 9; i++) //iterates over players in game settings
    {
        int playerDataIndex = convertBytesToInt(&d[offset]);
        if(playerDataIndex > MAX_PLAYERS)
        {
            logFunctionReturnsInt("analyseGameSettingsForCoops()", BAD_DATA, "Coop index found is greater than MAX_PLAYERS.", BASIC);
            return BAD_DATA;
        }

        offset += 4;
        log_(MODERATE, "Player data index: %d\n", playerDataIndex);
        int isHuman = convertBytesToInt(&d[offset]);    //0: invalid, 2: human, 4: computer
        log_(MODERATE, "isHuman: %d\n", isHuman);
        if(isHuman < 0 || COMPUTER < isHuman)  //1 has to be accepted as well. I found once in an older rec which was totally fine.
        {
            logFunctionReturnsInt("analyseGameSettingsForCoops()", BAD_DATA, "IsHuman has an invalid value.", BASIC);
            return BAD_DATA;
        }

        offset += 4;
        unsigned int nameLength = convertBytesToInt(&d[offset]);
        if(nameLength > 100)    //I dont know the real limit, but I have never seen a name longer than 100
        {
            logFunctionReturnsInt("analyseGameSettingsForCoops()", BAD_DATA, "Player name length is greater than 100.", BASIC);
            return BAD_DATA;
        }

        offset += 4;
        char nameBuf[100];
        convertBytesToString(&d[offset], nameBuf, nameLength);
        offset += nameLength;
        nameBuf[nameLength] = '\0';
        log_(MODERATE, "Player name: %s\n\n", nameBuf);

        if(HUMAN == isHuman || COMPUTER == isHuman)
        {
            Player* p = NULL;
            int j = 0, playerSaved = 0;
            for (j = 0; j < MAX_PLAYERS && !playerSaved; j++) //iterates over the players in the GameData
            {
                p = &pGameData->players[j];
                if(p->name != NULL)  //a player data is already saved on index j
                {
                    if(0 == strcmp(p->name, nameBuf)) //we found the same player, let's update its info with coop index and player number
                    {
                        p->number = i;
                        p->coopIndex = playerDataIndex;
                        playerSaved = 1;
                    }
                }
                else   //empty player slot found: we have found a new cooping player that is not saved yet
                {
                    p->name = (char*)malloc((nameLength + 1) * sizeof(char));   //+1 for the '\0'
                    memcpy(p->name, nameBuf, nameLength + 1);
                    p->number = i;
                    p->coopIndex = playerDataIndex;
                    pGameData->numPlayersWithoutGaia++;
                    pGameData->coopFound = 1;
                    setCivAndTeamForCoopingPlayer(p, pGameData);
                    playerSaved = 1;
                }
            }   //end for 8 players in game data
        }   //end if valid player
    }   //end for 9 player_info in game_setting
    log_(MODERATE, "Game data after coop analysis:\n");
    printGameData(pGameData, MODERATE);
    logFunctionReturnsInt("analyseGameSettingsForCoops()", OK, "", VERBOSE);
    return OK;
}

void setCivAndTeamForCoopingPlayer(Player* pPlayer, GameData* pGameData)
{
    logFunctionEntered("setCivAndTeamForCoopingPlayer()");

    Player* p = NULL;   //iterates over the players in game data
    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        p = &pGameData->players[i];
        if(pPlayer != p && pPlayer->coopIndex == p->coopIndex)  //found a player that has the same coopindex
        {
            if(p->civ != 0) //the civ is correctly set, we can copy data from this player (else it is another cooping player whixh is not set yet)
            {
                pPlayer->civ = p->civ;
                pPlayer->team = p->team;
                logFunctionReturnsVoid("setCivAndTeamForCoopingPlayer()");
                return;
            }
        }
    }
}

int readAchievements(byte* decompressedData, int startOffset, GameData* pGameData)
{
    logFunctionEntered("readAchievements()");
    int playerAchievementSize = 1473;
    int totalScoreOffset = startOffset + 13 + playerAchievementSize;
    byte* d = decompressedData;
    int i = 0;
    for (i = 1; i < pGameData->numPlayersWithoutGaia; i++) //we dont care about GAIA achievements, hence i = 1
    {
        printByteArray(&d[totalScoreOffset - 13], playerAchievementSize, VERBOSE);
        pGameData->players[i].totalScore = convertBytesToInt(&d[totalScoreOffset]);
        pGameData->players[i].militaryScore = convertBytesToInt(&d[totalScoreOffset + 30]);
        totalScoreOffset += playerAchievementSize;   //offset to the next players total score
    }
    logFunctionReturnsInt("readAchievements()", OK, "", VERBOSE);
    return OK;
}


