#ifndef HEADERANALYSING_H
#define HEADERANALYSING_H

#include <windows.h>
#include "GameDataStruct/gamedata.h"
#include "Zlib/zlib.h"
#include "logging.h"

/*! \file analyse_header.h Functions to analyse the header of the recorded game. */

/*!
 * \brief getHeaderData Decompresses the data passed to it and calls \see analyseHeaderData() to extract the info from it.
 * Uses zlib for decompression.
 * \param compressedData Pointer to the byte array that contains the compressed data.
 * \param compressedDataSize Size of the compressedData in bytes.
 * \param pGameData Pointer to a \see GameData struct. The extracted info will be stored to it.
 * \return \see OK if the decompression and analysis was successful, an error code otherwise.
 */
int getHeaderData(byte* compressedData, int compressedDataSize, GameData* pGameData);

/*!
 * \brief analyseHeaderData Gets the relevant info from the decompressed header.
 * Such as players, civilisations, diplomacy, map name, winners and losers.
 * \param decompressedData Pointer to the byte array that contains the decompressed data.
 * \param decompressedDataSize Size of the deCompressedData in bytes.
 * \param pGameData Pointer to a \see GameData struct. The extracted info will be stored to it.
 * \return \see OK if the decompression was successful, an error code otherwise.
 */
int analyseHeaderData(byte* decompressedData, int decompressedDataSize, GameData* pGameData);

/*!
 * \brief analysePlayerInfo Picks up the player names, civilisations and diplomacy from the data.
 * \param decompressedData Pointer to the byte array that contains the decompressed data.
 * \param decompressedDataSize Size of the deCompressedData in bytes.
 * \param startOffset The index in the decompressedData where the player info should start.
 * \param pGameData Pointer to a \see GameData struct. The extracted info will be stored to it.
 */
void analysePlayerInfo(byte* decompressedData, int decompressedDataSize, int startOffset, GameData* pGameData);

/*!
 * \brief analyseGameSettingsForCoops Finds cooping players and updates the already found ones with coopindex data.
 * Two players cooped if they have the same coopindex.
 * \param decompressedData Pointer to the byte array that contains the decompressed data.
 * \param startOffset The index in the decompressedData where the player info should start.
 * \param pGameData Pointer to a GameData struct. The extracted info will be stored to it.
 * \return \see OK if the analysis was successful, \see BAD_DATA otherwise.
 */
int analyseGameSettingsForCoops(byte* decompressedData, int startOffset, GameData* pGameData);

/*!
 * \brief setCivAndTeamForCoopingPlayer Helper function to set the civilisation and team for the newly found cooping players.
 * These values are not stored in the data, but they are always the same as the already found cooping partner's.
 * \param pPlayer Pointer to the player whose civ and team will be set.
 * \param pGameData Pointer to the \see GameData struct. The civ and team is read from it.
 */
void setCivAndTeamForCoopingPlayer(Player* pPlayer, GameData* pGameData);

/*!
 * \brief readAchievements This function should read the achievements of players.
 * Scores, economical achievements, military achievements etc. This function doesn't work, because the data is not found where the mgx reference
 * says.
 * \param decompressedData Pointer to the byte array that contains the decompressed data.
 * \param startOffset The index in the decompressedData where the player info should start.
 * \param pGameData Pointer to a GameData struct. The extracted info will be stored to it.
 * \return OK only, rest is not implemented.
 */
int readAchievements(byte* decompressedData, int startOffset, GameData* pGameData);


#endif // HEADERANALYSING_H
