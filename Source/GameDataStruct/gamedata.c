#include "stdafx.h"
#include "gamedata.h"
#include "enums.h"
#include "logging.h"

void initGameData(GameData* gd)
{
    logFunctionEntered("initGameData()");
    gd->povPlayer = 0;
    gd->numPlayersWithoutGaia = 0;
    gd->instructions = NULL;
    gd->coopFound = 0;

    int i = 0, j = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        gd->players[i].civ = 0;
        gd->players[i].militaryScore = 0;
        gd->players[i].name = NULL;
        gd->players[i].number = 0;
        gd->players[i].resigned = 0;
        gd->players[i].team = 0;
        gd->players[i].coopIndex = 0;
        gd->players[i].totalScore = 0;
        gd->players[i].pov = 0;
        for (j = 0; j < MAX_PLAYERS; j++)
        {
            gd->players[i].diplomacyFrom[j] = -1;
            gd->players[i].diplomacyTo[j] = -1;
        }
    }
    logFunctionReturnsVoid("initGameData()");
}

void finaliseGameData(GameData* gd)
{
    logFunctionEntered("finaliseGameData()");
    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        if(gd->players[i].name != NULL)
        {
            free(gd->players[i].name);
        }
    }
    free(gd->instructions);
    log_(VERBOSE, "Game data finalised.\n");
    logFunctionReturnsVoid("finaliseGameData()");
}
