#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <windows.h>
#include <stdio.h>
#include "constants.h"

/*! \file gamedata.h The Player and GameData structs are defined here with their init and finalise functions. */
/*!
\struct Struct that contains all the data extracted from a rec file.
*/
typedef struct
{
    byte number;    /*!< The player number. Must be between 1 and 8. */
    char* name;     /*!< The name of the player.*/
    byte pov;       /*!< Bool. 1 if the game was recorded from this player's point of view. 0 otherwise. */
    byte civ;       /*!< The ID of the civilisation. Must between 1 and 18 in classical AoC. \see civs in lookups.c */
    byte team;      /*!< Number of the team this player belongs to. Teams are between 1 and 4. 0 means no team. */
    byte resigned;    /*!< Boolean 0: winner, 1: loser */
    byte coopIndex;     /*!< Players that share the same coop index cooped. */
    int totalScore;     /*!< The total score the player had at the end of the game. This variable is not used currently. The data is not extracted. */
    int militaryScore;  /*!< The military score the player had at the end of the game. This variable is not used currently. The data is not extracted. */
    byte diplomacyFrom[MAX_PLAYERS];  /*!< Stores the other players' diplomatic stance to this player. For the possible values see the \see DiplomacyFrom enum in enums.h. */
    int diplomacyTo[MAX_PLAYERS]; /*!< Stores this player's diplomatic stance towards the other players. For the possible values see the \see DiplomacyTo enum in enums.h.
                            It is strange, that \diplomacyFrom diplomacyFrom and \diplomacyTo diplomacyTo have different types, but so is in the data*/
} Player;

/*! Struct that contains the players and the general game data.*/
typedef struct
{
    byte numPlayersWithoutGaia; /*! Number of players who participated in the game. Gaia doesn't count as a player. In the data Gaia does, so for example a 2v2 has 5 players. We save num_player - 1 here.*/
    short povPlayer;  /*! The number of the player whose point of view was this game recorded in. */
    Player players[MAX_PLAYERS];  /*! Holds the players. Not dynamic, because it is impossible to know how many there will be because of the coops */
    byte mapId; /*! ID of the map played. For possible values see \see getMapNameById in enums.c */
    char* instructions; /*! The instructions in the game. If the map is a custom map, the map name can be extracted from the instructions. */
    byte gameType;  /*! Contains a value from the \see GameType enum defined in enums.c */
    byte coopFound; /*! Boolean, 1 if there is at least one cooping player, 0 otherwise. */
} GameData;

/*!
 * \brief initGameData Initialises the variables of a \see GameData struct.
 * \param gd Pointer to the struct to be initialised.
 */
void initGameData(GameData* gd);

/*!
 * \brief finaliseGameData Finalises the game data. Frees the dynamically allocated data, such as the player names and the instructions.
 * \param gd Pointer to the struct to be finalised.
 */
void finaliseGameData(GameData* gd);


#endif // GAMEDATA_H
