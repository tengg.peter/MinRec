#include "stdafx.h"
#include "teambuilding_functions.h"
#include "helper.h"
#include "enums.h"

static byte povCoopIndex = 0;   //this is needed in comparePlayersByCoopIndexPOVTeam()


//this function works, I tested it. The array is not copied only referenced
void initTeamsArray(Player* array[MAX_PLAYERS][MAX_PLAYERS])
{
    logFunctionEntered("initTeamsArray()");

    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        int j = 0;
        for (j = 0; j < MAX_PLAYERS; j++)
        {
            array[i][j] = NULL;
        }
    }

    logFunctionReturnsVoid("initTeamsArray()");
}

void addPlayerToThisTeam(Player* pPlayer, Player* team[MAX_PLAYERS])
{
    logFunctionEntered("addPlayerToThisTeam()");

    int i=0;
    for (i = 0; i < MAX_PLAYERS; i++) //looks for the next empty slot within the team
    {
        if(NULL == team[i])  //empty slot, the player can be added here
        {
            team[i] = pPlayer;
            return;
        }
    }

    logFunctionReturnsVoid("addPlayerToThisTeam()");
}

void orderTeams(Player* teams[MAX_PLAYERS][MAX_PLAYERS], GameData* pGameData)
{
    logFunctionEntered("orderTeams()");

    Player* temp;
    int swapDone = 0, i = 0, j = 0;

    const Player* pPOVPlayer = getPOVPlayer(pGameData);

    log_(VERBOSE, "POV player's data:\n");
    printPlayerData(pPOVPlayer, VERBOSE);
    log_(MODERATE, "Teams before ordering:\n");
    printTeamsArray(teams, MODERATE);

    //finds the POV player in the array
    for (i = 0; i < MAX_PLAYERS && !swapDone; i++)
    {
        for (j = 0; j < MAX_PLAYERS && !swapDone; j++)
        {
            if(pPOVPlayer == teams[i][j])   //pointer comparison. No problem, if team[i][j] is NULL
            {
                if(i != 0)  //POV Player's team is not the first, teams have to be swapped
                {
                    for (j = 0; j < MAX_PLAYERS; j++) //j can be reused here, we won't iterate more on the array after swapping
                    {
                        temp = teams[0][j];
                        teams[0][j] = teams[i][j];
                        teams[i][j] = temp;
                    }
                    swapDone = 1;
                }
            }
        }
    }
    povCoopIndex = pPOVPlayer->coopIndex;
    //sorts the POV team by coopindex. The POV player has to be first
    qsort(teams[0], MAX_PLAYERS, sizeof(Player*), comparePlayersByCoopIndexPOVTeam);

    //simple sorting the other teams. It is not important who the first is
    for (i = 1; i < MAX_PLAYERS; i++)
    {
        qsort(teams[i], MAX_PLAYERS, sizeof(Player*), comparePlayersByCoopIndexNonPOVTeam);
    }

    log_(MODERATE, "Teams after ordering:\n");
    printTeamsArray(teams, MODERATE);

    logFunctionReturnsVoid("orderTeams()");
}

const Player* getPOVPlayer(const GameData* pGameData)
{
    logFunctionEntered("getPOVPlayer()");

    const Player* povPlayer = &pGameData->players[pGameData->povPlayer - 1];    //player 1 is on index 0.

    log_(VERBOSE, "<- getPOVPlayer() returns. POV player: %s\n", povPlayer->name);
    return povPlayer;
}

int comparePlayersByCoopIndexNonPOVTeam(const void* a, const void* b)
{
    logFunctionEntered("comparePlayersByCoopIndexNonPOVTeam()");
    log_(VERBOSE, "There are too many return statements here. You won't see a return message from this function.\n");

    Player* p1 = *(Player**)a;
    Player* p2 = *(Player**)b;

    if(NULL == p1 && NULL == p2)
        return 0;
    if(NULL == p1 && p2 != NULL)
        return 1;
    if(p1 != NULL && NULL == p2)
        return -1;

    if(strstr(p1->name, "Richard") != NULL)
        return -1;
    if(strstr(p2->name, "Richard") != NULL)
        return 1;

    return p1->coopIndex - p2->coopIndex;
}

int comparePlayersByCoopIndexPOVTeam(const void* a, const void* b)
{
    logFunctionEntered("comparePlayersByCoopIndexPOVTeam()");
    log_(VERBOSE, "There are too many return statements here. You won't see a return message from this function.\n");

    Player* p1 = *(Player**)a;
    Player* p2 = *(Player**)b;

    if(NULL == p1 && NULL == p2)
        return 0;
    if(NULL == p1&& p2 != NULL)
        return 1;
    if(p1 != NULL && NULL == p2)
        return -1;

    //POV player has the highest priority. Always has to be first
    if(p1->pov)
        return -1;
    if(p2->pov)
        return 1;

    //POV player's coop has the second highest priority
    if(p1->coopIndex == povCoopIndex && p2->coopIndex == povCoopIndex)
        return 0;
    if(p1->coopIndex == povCoopIndex)
        return -1;
    if(p2->coopIndex == povCoopIndex)
        return 1;

    //not pov player, normal comparison
    return p1->coopIndex - p2->coopIndex;

}

void buildTeamsArrayFromDiplomacy(GameData* pGameData, Player* teams[MAX_PLAYERS][MAX_PLAYERS])
{
    logFunctionEntered("buildTeamsArrayFromDiplomacy()");

    Player* pPlayerToPlace = NULL, *pPlayerAlreadyInTeam = NULL;
    byte teamFound = 0;

    //places the very first player in the array
    teams[0][0] = &pGameData->players[0];

    int i = 0, j = 0;
    for (i = 1; i < MAX_PLAYERS; i++)     //iterates over the players. Starts from 1, because player 0 is already put the the first place
    {
        pPlayerToPlace = &pGameData->players[i];
        if(NULL == pPlayerToPlace->name)    //no more player to place
            break;

        teamFound = 0;
        for (j = 0; j < MAX_PLAYERS && !teamFound; j++) //iterates over the teams. Always the first player is checked
        {
            pPlayerAlreadyInTeam = teams[j][0];
            if(NULL == pPlayerAlreadyInTeam)
            {
                //we have iterated over the exisitng teams, none was ally, starts a new one
                teams[j][0] = pPlayerToPlace;
                teamFound = 1;
                break;
            }

            if(areAllies(pPlayerToPlace, pPlayerAlreadyInTeam, pGameData))
            {
                //we found an ally
                addPlayerToThisTeam(pPlayerToPlace, teams[j]);
                teamFound = 1;
                break;
            }
        }
    }
    log_(MODERATE, "Teams built from diplomacy:\n");
    printTeamsArray(teams, MODERATE);
    logFunctionReturnsVoid("buildTeamsArrayFromDiplomacy()");
}

int getIndexOfPlayer(const Player* pPlayer, GameData* pGameData)
{
    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        if(&pGameData->players[i] == pPlayer)
            return i;
    }
    return -1;
}

int areAllies(const Player* p1, const Player* p2, GameData* pGameData)
{
    if(p1->coopIndex != 0 && p1->coopIndex == p2->coopIndex)    //cooping, they are the same team
    {
        return 1;
    }

    int playerIndex = getIndexOfPlayer(p2, pGameData);
    if(ALLY_FROM == p1->diplomacyFrom[playerIndex] && ALLY_TO == p1->diplomacyTo[playerIndex])
    {
        return 1;
    }
    return 0;
}
