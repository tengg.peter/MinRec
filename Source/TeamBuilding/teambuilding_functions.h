#ifndef TEAMBUILDINGFUNCTIONS_H
#define TEAMBUILDINGFUNCTIONS_H

#include "GameDataStruct\gamedata.h"
#include <windows.h>
#include <stdlib.h>
#include "logging.h"

/*! \file teambuilding_functions.h These functions are used to sort the player by teams and coops before putting the file name together */

/*!
 * \brief initTeamsArray Initialises a 8x8 teams array. Sets all the pointers to NULL.
 * \param array The array to be initialised.
 */
void initTeamsArray(Player* array[MAX_PLAYERS][MAX_PLAYERS]);

/*!
 * \brief addPlayerToThisTeam Adds a player to a team. Finds the first empty place in the team and sets that pointer to the player.
 * Players are not copied, they are in the \see GameData struct, only pointers are pointed at them.
 * \param pPlayer Pointer to the player to be added to the team.
 * \param team One of the teams from a 8x8 teams array. One row.
 */
void addPlayerToThisTeam(Player* pPlayer, Player* team[MAX_PLAYERS]);

/*!
 * \brief orderTeams Orders the teams and players ready for generating the file name. The Point Of View (POV) player is moved to the 0 index of
 * their own team, and the team is moved to the top. Then the teams are sorted by coop index, so cooping players get next to each other so that
 * they can be surrounded by parenthesis.
 * \param teams A 8x8 team array.
 * \param pGameData Pointer to the GameData struct. It is needed to find out who the POV player is.
 */
void orderTeams(Player* teams[MAX_PLAYERS][MAX_PLAYERS], GameData* pGameData);

/*!
 * \brief getPOVPlayer Returns the POV player from a \see GameData struct.
 * \param pGameData Pointer to a \see GameData struct.
 * \return
 */
const Player* getPOVPlayer(const GameData* pGameData);

/*!
 * \brief comparePlayersByCoopIndexNonPOVTeam This function is a comparer function used in the qsort to sort the players in the non POV team.
 * It simply orders the team by coopIndex. Little hack added ^^
 * \param a Pointer to one player.
 * \param b Pointer to the player.
 * \return -1 if a < b, 0 if a == b, 1 if a > b
 */
int comparePlayersByCoopIndexNonPOVTeam(const void* a, const void* b);

/*!
 * \brief comparePlayersByCoopIndexPOVTeam This function is a comparer function used in the qsort to sort the players in the POV team.
 * The POV player is always returned as the smallest so it will be at the beginning of the team followed by their coops. The rest is ordered
 * by coop index.
 * \param a Pointer to one player.
 * \param b Pointer to the player.
 * \return -1 if a < b, 0 if a == b, 1 if a > b
 */
int comparePlayersByCoopIndexPOVTeam(const void* a, const void* b);

/*!
 * \brief buildTeamsArrayFromDiplomacy This function places the players into the 8x8 teams array using he diplomacy data.
 * It is necessary because in cooping games the teams are not stored correctly in the data.
 * \param pGameData Pointer to the \see GameData struct.
 * \param teams The 8x8 teams array containing Player pointers. The pointers will be set to the players in the GameData. Allied players
 * will be in the same team (row in the 2d array)
 */
void buildTeamsArrayFromDiplomacy(GameData* pGameData, Player* teams[MAX_PLAYERS][MAX_PLAYERS]);

/*!
 * \brief getIndexOfPlayer Gets the index of a given player from the GameData. The GameData struct stores the players in an array. The index returned
 * indexes the player in that array.
 * \param pPlayer Pointer to the player whose index will be returned.
 * \param pGameData
 * \return An index between 0 and 7. Returns -1 if the player is not in the data. This should never happen.
 */
int getIndexOfPlayer(const Player* pPlayer, GameData* pGameData);

/*!
 * \brief areAllies Checks if two players are allies, that is they are in the same team.
 * \param p1 Pointer to the first \see Player.
 * \param p2 Pointer to the second \see Player.
 * \param pGameData Pointer to the \see GameData struct.
 * \return 1 if they are allies, 0 otherwise.
 */
int areAllies(const Player* p1, const Player* p2, GameData* pGameData);

#endif // TEAMBUILDINGFUNCTIONS_H
