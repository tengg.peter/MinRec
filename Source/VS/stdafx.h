// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
#include "Analyse\analyse_body.h"
#include "Analyse\analyse_header.h"

#include "GameDataStruct\gamedata.h"

#include "TeamBuilding\teambuilding_functions.h"

#include "byteconversions.h"
#include "constants.h"
#include "enums.h"
#include "helper.h"
#include "locatefunctions.h"
#include "logging.h"
#include "lookups.h"
#include "rename.h"
#include "tests.h"
#include "zlibfunctions.h"
