#include "stdafx.h"
#include "byteconversions.h"
#include "enums.h"
#include "logging.h"

int convertBytesToInt(byte* firstByte)
{
    byte* a = firstByte;
    int i = a[3] << 24 | a[2] << 16 | a[1] << 8 | a[0];

    return i;
}

short convertBytesToShort(byte* firstByte)
{
    byte* a = firstByte;
    short s = a[1] << 8 | a[0];
    return s;
}

char* convertBytesToString(byte* firstByte, char* buf, int length)
{
    int i = 0;
    for (i = 0; i < length; i++)
    {
        buf[i] = firstByte[i];
    }

    return buf;
}
