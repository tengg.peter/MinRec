#ifndef BYTECONVERSIONS_H
#define BYTECONVERSIONS_H

#include <windows.h>

/*! \file byteconversions.h Functions to convert the byte stream from the file to other types */

/*!
 * \brief convertBytesToInt Converts a sequence of bytes to a 4 bytes int. The dataformat in the mgx files are little endian.
 * \param firstByte Pointer to the first byte. The int will be created from the following 4 bytes including firstByte.
 * \return The int value encoded by the 4 input bytes.
 */
int convertBytesToInt(byte* firstByte);

/*!
 * \brief convertBytesToShort Converts a sequence of bytes to 2 bytes short. The dataformat in the mgx files are little endian.
 * \param firstByte Pointer to the first byte. The short will be created from the following 2 bytes including firstByte.
 * \return The short value encoded by the 2 input bytes.
 */
short convertBytesToShort(byte* firstByte);

/*!
 * \brief convertBytesToString Converts a sequence of bytes to char* string. The strings in the mgx files are one byte ascii characters.
 * \param firstByte Pointer to the first byte.
 * \param buf Pointer to a char buffer to which the string will be written. It is the caller's responisiblity to provide a big enough buffer.
 * \param length Length of the string to be extracted from the data.
 * \return The pointer to buf. Same as it was got in \buf.
 */
char* convertBytesToString(byte* firstByte, char* buf, int length);

#endif // BYTECONVERSIONS_H
