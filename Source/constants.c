#include "stdafx.h"
#include "constants.h"

const byte LOG_TO_CONSOLE = 1;
const byte LOG_TO_FILE = 1;

const LogLevel LOG_LEVEL = BASIC;

const char* LOG_FILE_NAME_FORMAT = "Log - %d-%02d-%02d %02d'%02d'%02d.txt";
