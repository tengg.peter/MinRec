#include <windows.h>    //include for the byte type
#include "enums.h"

#ifndef FLAGS_H
#define FLAGS_H

/*! \file constants.h Constants used globally in the program. */

#define MAX_PLAYERS 8

/*!
 * \brief LOG_TO_CONSOLE The program logs to the console if it has a value other than 0.
 */
extern const byte LOG_TO_CONSOLE;
/*!
 * \brief LOG_TO_FILE The program logs to text file if it has a value other than 0.
 */
extern const byte LOG_TO_FILE;

/*!
 * \brief LOG_LEVEL Specifies the detail of logs. For possible values \see \LogLevel in enums.h
 */
extern const LogLevel LOG_LEVEL;

/*!
 * \brief LOG_FILE_NAME_FORMAT The format of the names of the log files. For the format see the \see constants.c file.
 */
extern const char* LOG_FILE_NAME_FORMAT;

#endif // FLAGS_H
