#ifndef ENUMS_H
#define ENUMS_H

/*! \file enums.h Enums used throughout the program are defined here. */

/*! \enum DiplomacyFrom These are the values used in the data to specify the diplomacy from other players. They are used as ints directly. */
typedef enum
{
    ALLY_FROM = 0,  /*!< The player on the given index is ally towards this player. Every player is ally to themselves. */
    ENEMY_FROM = 3  /*!< The player on the given index is enemy towards this player. */
} DiplomacyFrom;

/*! \enum DiplomacyTo These are the values used in the data to specify the the diplomacy towards other players. They are used as ints directly. */
typedef enum
{
    INVALID_PLAYER_TO = -1,  /*!< If the given player is invalid, there is -1 at their index. For example, if the game is 2v2, the last 4 players are invalid */
    GAIA_TO = 0,    /*!< Every player is always 0 towards Gaia. */
    SELF_TO = 1,    /*!< There is always 1 at their own index. */
    ALLY_TO = 2,    /*!< This player is ally towards the indexed player. */
    NEUTRAL_TO = 3, /*!< This player is neutral towards the indexed player. */
    ENEMY_TO = 4   /*!< This player is enemy towards the indexed player. */
} DiplomacyTo;

/*! \enum IsHuman used in the game settings, where coops are discovered. These values are used directly as integers. */
typedef enum
{
    INVALID = 0,    /*!< Unused player slot. */
    HUMAN = 2,      /*!< Human player. */
    COMPUTER = 4    /*!< Computer player. */
} IsHuman;

/*! \enum LogLevel having room between existing levels for potential future levels */
typedef enum
{
    OFF = 0,        /*!< The program does not log anything. */
    BASIC = 5,      /*!< Original and new filenames and errors are logged. */
    MODERATE = 10,  /*!< BASIC + prints the data extracted from the file. */
    VERBOSE = 15    /*!< MODERATE + prints the entries and return values of most functions. Prints parts of the raw data in some cases. */
} LogLevel;

/*! \enum GameType The game type is encoded in the rec file using the following constants. */
typedef enum
{
    RANDOM_MAP = 0,
    REGICIDE = 1,
    DEATH_MATCH = 2,
    SCENARIO = 3,
    CAMPAIGN = 4,
    KING_OF_THE_HILL = 5,
    WONDER_RACE = 6,
    DEFEND_THE_WONDER = 7,
    TURBO_RANDOM = 8
} GameType;

/*! \enum RetVal Return values used by functions that return int. */
typedef enum
{
    OK = 0, /*! Everything went fine, the function ran successfully. */
    BAD_DATA = 1,   /*! Something went wrong during analysis. If this value is returned, the program cannot continue processing the data and moves to the next file. */
    NULL_PARAMETER = 2  /*! Error. One of the parameters were NULL. The program cannot continue. */
} RetVal;


#endif // ENUMS_H
