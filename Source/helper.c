#include "stdafx.h"
#include "helper.h"
#include "byteconversions.h"
#include <stdio.h>
#include "logging.h"
#include "lookups.h"

void printByteArray(byte* array, int size, LogLevel level)
{
    int i;
    for(i = 0; i < size; i++)
    {
        log_(level, "%02x ", array[i]);
    }
    log_(level, "\n");
}

void printByteArrayGrouped(byte* array, int arraySize, int* grouping, int groupingSize, LogLevel level)
{
    int i = 0, groupIndex = 0, printedInGroup = 0;

    for (i = 0; i < arraySize;)
    {
        if(groupIndex < groupingSize && 0 == printedInGroup)    //print the size before the group
        {
            log_(level, "\n%d: ", grouping[groupIndex]);
        }
        if(printedInGroup < grouping[groupIndex])
        {
            log_(level, "%02x ", array[i++]);
            printedInGroup++;
        }

        if(groupIndex < groupingSize)    //we still have to cate about the grouped printing
        {
            if(printedInGroup >= grouping[groupIndex])    //did not finish the current group yet
            {
                groupIndex++;
                printedInGroup = 0;
                if(groupIndex >= groupingSize)
                {
                    log_(level, "\n");  //new line after the last group
                }
            }
        }
        else    //end of grouping, just print the rest
        {
            log_(level, "%02x ", array[i++]);
        }
    }
    log_(level, "\n");
}

void printString(byte* array, int size, LogLevel level)
{
    int i=0;
    for (i = 0; i < size; i++)
    {
        log_(level, "%c", array[i]);
    }
    log_(level, "\n");
}

void scanForInts(byte* array, int from, int to, LogLevel level)
{
    int i = 0;
    byte* a = array;
    for (i = from; i <= to - 2; i++)
    {
        log_(level, "%d: %02x %02x %02x %02x: %d\n", i - from, a[i], a[i+1], a[i+2], a[i+3], convertBytesToInt(&a[i]));
    }
}

void scanForShorts(byte* array, int from, int to, LogLevel level)
{
    int i = 0;
    byte* a = array;
    for (i = from; i <= to; i++)
    {
        log_(level, "%d: %02x %02x: %d\n", i - from, a[i], a[i+1], convertBytesToShort(&a[i]));
    }
}

void scanForChars(byte* array, int from, int to, LogLevel level)
{
    int i = 0;
    byte* a = array;
    for (i = from; i <= to; i++)
    {
        log_(level, "%d: %02x: %c\n", i - from, a[i], a[i]);
    }
}

int findPattern(byte* array, byte* pattern, int patternLength, int from, int to, byte printUnmatchedBytes, LogLevel level)
{
    if(0 == patternLength)
        return -1;

    log_(level, "Pattern to find: ");
    int k = 0;
    for (k = 0; k < patternLength; k++)
    {
        log_(level, "%02x ", pattern[k]);
    }
    log_(level, "\n");

    HANDLE hConsole;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    int yellowColour = 14, whiteColour = 7, matched = 0, retval = -1;
    byte* a = array;

    int i = 0;
    for (i = from; i <= to; i++)
    {
        if(0 == matched && a[i] == pattern[0])  //first match
        {
            matched = 1;
        }
        else if(matched > 0 && matched < patternLength)   //we have some matches already
        {
            if(a[i] == pattern[matched])    //matched again, increase matched count
            {
                matched++;
            }
            else    //not matched, pattern not found, we have to print the bytes not printed before
            {
                if(printUnmatchedBytes)
                {
                    int j;
                    for (j = matched; j > 0; j--)
                    {
                        log_(level, "%02x ", a[i - j]);
                    }
                }
                matched = 0;
            }
        }
        else if(matched == patternLength)   //full pattern found
        {

            SetConsoleTextAttribute(hConsole, yellowColour);
            int j;
            for (j = patternLength; j > 0; j--)
            {
                log_(level, "%02x ", a[i - j]);
            }
            SetConsoleTextAttribute(hConsole, whiteColour);

            matched = 0;
            if(-1 == retval)
            {
                retval = i;
            }
        }
        else    //not matched, random byte
        {
            if(printUnmatchedBytes)
            {
                log_(level, "%02x ", a[i]);
            }
        }
    }
    log_(level, "\n");
    if(retval != -1)
    {
        log_(level, "Pattern found: %d \n", retval);
    }
    else
    {
        log_(level, "Pattern not found!\n");
    }
    return retval;
}

int seekPattern(byte* array, byte* pattern, int patternLength, int from, int to)
{
    logFunctionEntered("seekPattern()");

    int i = 0, matched = 0;
    byte* a = array;

    log_(VERBOSE, "pattern to find: ");
    printByteArray(pattern, patternLength, VERBOSE);
    log_(VERBOSE, "\n");

    for (i = from; i <= to - patternLength; i++)
    {
        if(0 == matched && a[i] == pattern[0])  //first match
        {
            matched = 1;
        }
        else if(matched > 0 && matched < patternLength)   //we have some matches already
        {
            if(a[i] == pattern[matched])    //matched again, increase matched count
            {
                matched++;
            }
            else    //not matched, pattern not found, we have to print the bytes not printed before
            {
                matched = 0;
            }
        }
        else if(matched == patternLength)   //full pattern found
        {
            log_(VERBOSE, "<- seekPattern() returns. Pattern found at %d\n", i);
            return i - patternLength;    //returns the index where the pattern begins
        }
    }
    log_(VERBOSE, "<- seekPattern() returns -1. Pattern not found.");
    return -1;
}

wchar_t* getExtension(const wchar_t* fileName)
{
    return wcsrchr(fileName, L'.');
}

void printGameData(const GameData* pGameData, LogLevel level)
{
    int i = 0;
    log_(level, "\nbegin game data ====================\n");
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        printPlayerData(&pGameData->players[i], level);
    }
    log_(level, "num players: %d\n", pGameData->numPlayersWithoutGaia);
    log_(level, "POV player: %d\n", pGameData->povPlayer);
    log_(level, "map id: %d\n", pGameData->mapId);
    log_(level, "game type: %d\n", pGameData->gameType);
    log_(level, "coop found: %d\n", pGameData->coopFound);
    log_(level, "%s\n", pGameData->instructions);
    log_(level, "\nend game data ====================\n\n");
}

void printPlayerData(const Player* pPlayer, LogLevel level)
{
    if(pPlayer->name != NULL)
    {
        log_(level, "%s\n", pPlayer->name);
        log_(level, "palyer number: %d\n", pPlayer->number);
        log_(level, "coop index: %d\n", pPlayer->coopIndex);
        log_(level, "civ: %s\n", civs[pPlayer->civ]);
        log_(level, "team: %d\n", pPlayer->team);
        log_(level, "resigned: %d\n", pPlayer->resigned);
        log_(level, "diplomacy from: ");
        int i = 0;
        for (i = 0; i < MAX_PLAYERS; i++)
        {
            log_(level, "%d ", pPlayer->diplomacyFrom[i]);
        }
        log_(level, "\ndiplomacy to: ");
        for (i = 0; i < MAX_PLAYERS; i++)
        {
            log_(level, "%d ", pPlayer->diplomacyTo[i]);
        }
        log_(level, "\n\n");
    }
}

void printTeamsArray(Player* teams[MAX_PLAYERS][MAX_PLAYERS], LogLevel level)
{
    log_(level, "\n");
    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        printOneTeam(teams[i], level);
    }
    log_(level, "\n");
}

void printOneTeam(Player* team[MAX_PLAYERS], LogLevel level)
{
    int j = 0;
    for (j = 0; j < MAX_PLAYERS; j++)
    {
        if(NULL == team[j])
            log_(level, "NULL ");
        else if(NULL == team[j]->name)
            log_(level, "noname ");
        else
            log_(level, "%s ", team[j]->name);
    }
    log_(level, "\n");
}
