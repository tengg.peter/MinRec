#ifndef HELPER_H
#define HELPER_H

#include <windows.h>
#include "GameDataStruct/gamedata.h"
#include "logging.h"

/*! \file helper.h The functions declared here are mostly used for debugging. */

/*!
 * \brief printByteArray Prints a byte array from the data as two digit hexadecimal numbers.
 * \param array Pointer to the first byte.
 * \param size Number of bytes to be printed.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printByteArray(byte* array, int size, LogLevel level);

/*!
 * \brief printByteArrayGrouped Prints a groupped byte array from the data as two digit hexadecimal numbers. Each group of bytes is printed
 * to a new line.
 * \param array Pointer to the first byte.
 * \param arraySize Number of bytes to be printed.
 * \param grouping Pointer to an array of ints. Each int in this array will be a size of a group.
 * E.g.: 5,4,2: the first group will contain 5 bytes, the second 4, etc... If there are any remaining bytes at the end, they are printed as one group.
 * \param groupingSize Length of the grouping array.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printByteArrayGrouped(byte* array, int arraySize, int* grouping, int groupingSize, LogLevel level);

/*!
 * \brief printString Prints a byte array as one byte ascii characters.
 * \param array Pointer to the first byte.
 * \param size Number of bytes to be printed.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printString(byte* array, int size, LogLevel level);

/*!
 * \brief scanForInts Moves through the byte array byte by byte and prints each 4 bytes as ints. Useful for searching for an int value, when its
 * exact location is unknown.
 * \param array Pointer to the first byte.
 * \param from The index where the scanning starts.
 * \param to The index where the scanning ends. The last byte of the last int will be at this index. The scanning does not move beyond this index.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void scanForInts(byte* array, int from, int to, LogLevel level);

/*!
 * \brief scanForShorts Moves through the byte array byte by byte and prints each 2 bytes as shorts. Useful for searching for an short value, when its
 * exact location is unknown.
 * \param array Pointer to the first byte.
 * \param from The index where the scanning starts.
 * \param to The index where the scanning ends. The last byte of the last short will be at this index. The scanning does not move beyond this index.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void scanForShorts(byte* array, int from, int to, LogLevel level);

/*!
 * \brief scanForChars Moves through the byte array byte by byte and prints each byte as ascii character. Useful when searching for strings.
 * \param array Pointer to the first byte.
 * \param from The index where the scanning starts.
 * \param to The index where the scanning ends.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void scanForChars(byte* array, int from, int to, LogLevel level);

/*!
 * \brief findPattern Searches for a pattern in the byte stream. When matched, it highlights it in yellow.
 * \param array Pointer to the first byte.
 * \param pattern Pointer to the first byte of the pattern.
 * \param patternLength Number of bytes of the pattern.
 * \param from The index where the scanning starts.
 * \param to The index where the scanning ends. The pattern searched for must end within this index, it cannot extend over it.
 * \param printUnmatchedBytes If 0, it prints only the pattern if found. Else it prints all bytes that do not match the pattern too.
 * The pattern itself is always printed.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 * \return The index of the first occurance of the pattern. 0 if it was not found.
 */
int findPattern(byte* array, byte* pattern, int patternLength, int from, int to, byte printUnmatchedBytes, LogLevel level);

/*!
 * \brief seekPattern Finds the first occurance of a pattern and returns its index.
 * \param array Pointer to the first byte.
 * \param pattern Pointer to the first byte of the pattern.
 * \param patternLength Number of bytes of the pattern.
 * \param from The index where the scanning starts.
 * \param to The index where the scanning ends. The pattern searched for must end within this index, it cannot extend over it.
 * \return The index of the first occurance of the pattern. 0 if it was not found.
 */
int seekPattern(byte* array, byte* pattern, int patternLength, int from, int to);

/*!
 * \brief getExtension Returns a pointer to the index where the last '.' is found in the string. It is used to get the pointer to the extension
 * in file names.
 * \param fileName Pointer to the file name.
 * \return
 */
wchar_t* getExtension(const wchar_t* fileName);

/*!
 * \brief printGameData Prints all member variables of a \see GameData struct including Players and their variables.
 * \param pGameData Pointer to the \see GameData struct.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printGameData(const GameData* pGameData, LogLevel level);

/*!
 * \brief printPlayerData Prints all member variables of a \see Player struct.
 * \param pPlayer Pointer to the \see Player struct.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printPlayerData(const Player* pPlayer, LogLevel level);

/*!
 * \brief printTeamsArray Prints a 8x8 teams array. One team per line. Uses \see printOneTeam()
 * \param teams The 8x8 Player* array.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printTeamsArray(Player* teams[MAX_PLAYERS][MAX_PLAYERS], LogLevel level);

/*!
 * \brief printOneTeam Prints a team array in one row. Prints "NULL" if the pointer is NULL, "noname", if the player's name is NULL or the player's name
 * \param team A 8 long Player* array.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void printOneTeam(Player* team[MAX_PLAYERS], LogLevel level);

#endif // HELPER_H
