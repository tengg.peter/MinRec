#include "stdafx.h"
#include "locatefunctions.h"
#include "helper.h"
#include "byteconversions.h"
#include "logging.h"
#include "lookups.h"

int countAIInfoLength(byte* decompressedData, int startOffset)
{
    logFunctionEntered("countAIInfoLength()");

    int offset = startOffset;
    int i = 0;  //loop indices
    byte* d = decompressedData;

    //AI info =================================================================================================================================
    //String table section
    offset += 2;    //unknown short
    short numString = convertBytesToShort(&d[offset]);  //18

    //Resource_string
    offset = 24;
    for (i = 0; i < numString; i++)
    {
        int stringLength = convertBytesToInt(&d[offset]);
        offset += 4;
        offset += stringLength;
    }
    //Resource_string end

    offset += 6;    //const 6 bytes
    //String table end

    //AI data
    for (i = 0; i < MAX_PLAYERS; i++) //always 8 times
    {
        offset += 10;
        short numRule = convertBytesToShort(&d[offset]);
        offset += 6;
        offset += numRule * (16 + 16 * 24); //for each rule: 16 constant bytes plus 16 times 24 bytes
    }
    //AI data end

    offset += 104;  //unkown[104]
    offset += 8 * 10 * 4;   //timers
    offset += 1024; //shared_goal
    offset += 4096; //"zero" int[1024]
    //AI info end =============================================================================================================================

    int retVal = offset - startOffset;
    logFunctionReturnsInt("countAIInfoLength()", retVal, "Returns AI info length.", VERBOSE);
    return retVal; //16 bytes before do not belong to the AI data.
}

int locateScenarioHeader(byte* decompressedData, int decompressedDataSize, int startOffset)
{
    logFunctionEntered("locateScenarioHeader()");

    byte* d = decompressedData;
    byte* bPtr = d;
    int offset = startOffset;
    int numPatternFound = 0, matched = 0;

    while(offset < decompressedDataSize - 256)  //256 is 16*16
    {
        bPtr = &d[offset];
        matched = isScenarioHeaderPattern(bPtr);    //TODO: this pattern matching is bad. It works only by luck.
        if(1 == matched)
        {
            numPatternFound++;
            while (1 == matched)
            {
                bPtr += 16;
                matched = isScenarioHeaderPattern(bPtr);
                if(1 == matched)
                {
                    numPatternFound++;
                    if(16 == numPatternFound)
                    {
                        int retVal = offset - 12 - 4096 - 64 - 8; //the Scenario Header begins there
                        log_(VERBOSE, "<- locateScenarioHeader() returns. Scenario header found at %d\n", retVal);
                        return retVal;
                    }
                }
                else    //not matched
                {
                    numPatternFound = 0;
                }
            }
        }
        offset++;
    }
    log_(BASIC, "<- locateScenarioHeader() returns %d. Scenario header not found. Bad data.\n");
    return BAD_DATA;
}

int isScenarioHeaderPattern(byte* ptrTo0x04)
{
    int constant0x04 = convertBytesToInt(ptrTo0x04);
    if(constant0x04 != 4)     //must be 4
        return 0;

    int civilisation = convertBytesToInt(ptrTo0x04 - 4);
    if(civilisation < 0 && civilisation > 16)     //must be between 1 and 16
        return 0;

    int human = convertBytesToInt(ptrTo0x04 - 8);
    if(human != 0 && human != 1)  //must be 0 or 1
        return 0;

    int active = convertBytesToInt(ptrTo0x04 - 12);
    if(active != 0 && active != 1)    //must be 0 or 1
        return 0;

//    if(active && human)
//    {
//        printByteArray(ptrTo0x04 - 12, 16, BASIC);
//        log(BASIC, "civilisation: %d\n", civilisation);
//    }

    return 1;
}

int locateInstructions(byte* decompressedData, int scenarioHeaderOffset)
{
    logFunctionEntered("locateInstructions()");

    byte* d = decompressedData;
    int offset = scenarioHeaderOffset + 4433;   //+ all the constant length data
    unsigned short stringLength = convertBytesToShort(&d[offset]);
    offset += 2 + stringLength; //short + length

    log_(VERBOSE, "<- locateInstructions() returns. Instructions found at %d\n", offset);
    return offset;
}

int locateGameSettings(byte* decompressedData, int decompressedDataSize, int startOffset)
{
    logFunctionEntered("locateGameSettings()");

    byte pattern[] = { 0x9d, 0xff, 0xff, 0xff };    //we have to find the last occurence of this pattern
    int offset, lastOffset = -1;

    while(1)
    {
        offset = seekPattern(decompressedData, pattern, 4, startOffset, decompressedDataSize);
        if(-1 == offset)
        {
            log_(VERBOSE, "<- locateGameSettings() returnes. Game settings located at %d\n", lastOffset);
            return lastOffset;
        }
        lastOffset = offset;
        startOffset = lastOffset + 4;
    }
}

//not used, the trigger info is too complicated. Voobly hacked it.

//int stepOverTriggerInfo(byte* decompressedData, int decompressedDataSize, int offset_9A_99_99_99_99_99_F9_3F)
//{
//    int offset = offset_9A_99_99_99_99_99_F9_3F, printOffset = 0;
//    byte* d = decompressedData;
//    offset += 9;
//    int numTriggers = convertBytesToInt(&d[offset]);
//    log("numTriggers: %d\n", numTriggers);
//    offset += 4;

//    //triggers
//    int i = 0;
//    for (i = 0; i < numTriggers; i++)
//    {
//        printOffset = offset;
//        offset += 14;
//        int descLen = convertBytesToInt(&d[offset]);
//        log("descLen: %d\n", descLen);
//        offset += 4;
//        offset += descLen;  //description: length chars
//        int nameLen = convertBytesToInt(&d[offset]);
//        log("nameLen: %d\n", nameLen);
//        offset += 4;
//        offset += nameLen;

//        int vooblyLen = 0;
//        int v = 0;
//        for (v = 0; v < i+1; v++)
//        {
//            vooblyLen = convertBytesToInt(&d[offset]);
//            log("voobly: %d\n", vooblyLen);
//            offset += 4 + vooblyLen;
//        }

//        int numEffect = convertBytesToInt(&d[offset]);
//        log("numEffect: %d\n", numEffect);
//        offset += 4;

//        int triggerGrouping[] = {4,1,1,4,4,4, descLen,4, nameLen, 4, vooblyLen,4};
//        printByteArrayGrouped(&d[printOffset], 400, &triggerGrouping[0], 12);



//        //effects
//        int j = 0;
//        for (j = 0; j < numEffect; j++)
//        {
//            int type = convertBytesToInt(&d[offset]);
//            int check = convertBytesToInt(&d[offset + 4]);

//            log("type: %d\n", type);
//            log("check: %d\n", check);

//            printOffset = offset;
//            offset += 24;
//            int numSelectedObject = convertBytesToInt(&d[offset]);
//            numSelectedObject = numSelectedObject < 0 ? 0 : numSelectedObject;
//            offset += 40 + 24 + 12;
//            int textLen = convertBytesToInt(&d[offset]);
//            log("textLen: %d\n", textLen);
//            offset += 4 + textLen;  //textLen

//            int soundLen = convertBytesToInt(&d[offset]);
//            log("soundLen: %d\n", soundLen);
//            offset += 4 + soundLen;
//            offset += numSelectedObject * 2;    //selected unit ids, shorts

//            int grouping[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
//                              8, 8, 8, 4, 4, 4, 4, textLen, 4, soundLen, numSelectedObject};
//            printByteArrayGrouped(&d[printOffset], 400, &grouping[0], 27);
//        }
//        offset += numEffect * 4;    //effect orders

//        //conditions
//        int numCond = convertBytesToInt(&d[offset]);
//        log("numCond: %d\n", numCond);
//        offset += numCond * (44 + 16 + 12);
//        offset += numCond * 4;  //condition orders
//    }
//    offset += numTriggers * 4;

//    return offset;
//}

int locateOtherData(byte* decompressedData, int decompressedDataSize)
{
    logFunctionEntered("locateOtherData()");

    byte* d = decompressedData;
    int i = decompressedDataSize - 1 - 31;  //31 is the length of the pattern we are looking for
    int matched = 1, offset = 0;

    for (; i >= 0; i--)
    {
        matched = 1;
        //checking the team array:
        int j = 0;
        for (j = 0; j < MAX_PLAYERS; j++)
        {
            if(d[i+j] < 1 || 5 < d[i+j])
            {
                matched = 0;
                break;
            }
        }
        if(0 == matched)
        {
            continue;
        }

        //checking the pop limit
        offset = i + 21;
        if(d[offset] < 1 || 10 < d[offset])
        {
            continue;
        }

        //checking the game type
        offset += 4;
        if(8 < d[offset])   //byte type cannot be less than 0, no need to check that
        {
            continue;
        }

        //checking lock diplomacy
        offset++;
        if(d[offset] != 0 && d[offset] != 1)    //must be a bool value
        {
            continue;
        }

        //checking the number of pre-game chat
        offset++;
        int numChat = convertBytesToInt(&d[offset]);
        if(numChat < 0 || 50 < numChat)
        {
            continue;
        }

        //pattern found at i.
        log_(VERBOSE, "<- locateOtherData() returns. Other data located at %d\n\n", i);
        return i;
    }
    logFunctionReturnsInt("locateOtherData()", -1, "Other data not found.", BASIC);
    return -1;
}
