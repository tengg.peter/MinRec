#ifndef LOCATEFUNCTIONS_H
#define LOCATEFUNCTIONS_H

#include <windows.h>

/*! \file locatefunctions.h Functions that are used to find certain sections of the data and get their starting index. */

/*!
 * \brief countAIInfoLength Counts the length of the AI info from the specified start index. We are not interested in the AI inffo,
 * we want to step over it. This function counts it length and returns the index where it ends.
 * \param decompressedData Pointer to the byte array that holds the data.
 * \param startOffset The index where the AI info starts.
 * \return The length in bytes of the AI info length.
 */
int countAIInfoLength(byte* decompressedData, int startOffset);

/*!
 * \brief locateScenarioHeader Finds where the scenario header file starts and returns its offset.
 * \param decompressedData Pointer to the byte array that holds the data.
 * \param decompressedDataSize Size in bytes of the data.
 * \param startOffset The index where the search starts.
 * \return The index where the scenario header is found. BAD_DATA if it wasn't found.
 */
int locateScenarioHeader(byte* decompressedData, int decompressedDataSize, int startOffset);

/*!
 * \brief isScenarioHeaderPattern This is a very specific function used in \see locateScenarioHeader(). It gets a pointer to a byte and
 * matches the pointed data to a specific pattern. It is player_data1 inside the scenario header. active, human, civilization and the constant 0x04
 * \param ptrTo0x04 Pointer to a byte where the pattern will be checked.
 * \return 1 if the pattern was matched, 0 otherwise.
 */
int isScenarioHeaderPattern(byte* ptrTo0x04);

/*!
 * \brief locateInstructions Finds the instructions and returns its starting index.
 * \param decompressedData Pointer to the byte array that holds the data.
 * \param scenarioHeaderOffset Index where the secnaro header starts. The instructions follow this block.
 * \return The index where the instructions start.
 */
int locateInstructions(byte* decompressedData, int scenarioHeaderOffset);

/*!
 * \brief locateGameSettings Finds the game settings and returns its starting index.
 * \param decompressedData Pointer to the byte array that holds the data.
 * \param decompressedDataSize Total length of the data in bytes.
 * \param startOffset The index where the search starts.
 * \return The index where the game settings start.
 */
int locateGameSettings(byte* decompressedData, int decompressedDataSize, int startOffset);

//this is an interesting function with some potentially useful code, so I decided not to remove it completely
//int stepOverTriggerInfo(byte* decompressedData, int decompressedDataSize, int offset_9A_99_99_99_99_99_F9_3F);

/*!
 * \brief locateOtherData Finds the other data and returns its starting index.
 * \param decompressedData Pointer to the byte array that holds the data.
 * \param decompressedDataSize Total length of the data in bytes.
 * \return The index where the other data starts.
 */
int locateOtherData(byte* decompressedData, int decompressedDataSize);

#endif // LOCATEFUNCTIONS_H
