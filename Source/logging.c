#include "stdafx.h"
#include "logging.h"

static char fileNameBuf[35] = "\0"; //Log - Wed Feb 13 17:17:11 2013.txt (length: 34+1)

void log_(LogLevel level, const char* format, ...)
{
    if(!LOG_TO_CONSOLE && !LOG_TO_FILE)   //checking the settings
        return;

    if(LOG_LEVEL < level)
        return;

    va_list args;
    va_start(args, format);

    if(LOG_TO_CONSOLE)
    {
        //console:
        vprintf(format, args);
    }

    if(LOG_TO_FILE)
    {
        if(0 == strlen(fileNameBuf))    //log file name not initialised yet
        {
            time_t rawtime;
            struct tm * timeinfo;

            time (&rawtime);
            timeinfo = localtime(&rawtime);
            sprintf(&fileNameBuf[0], LOG_FILE_NAME_FORMAT,
                    timeinfo->tm_year + 1900,
                    timeinfo->tm_mon + 1,
                    timeinfo->tm_mday,
                    timeinfo->tm_hour,
                    timeinfo->tm_min,
                    timeinfo->tm_sec);
        }

        //file:
        FILE* fp = fopen(fileNameBuf, "a");
        vfprintf(fp, format, args);
        fclose(fp);
    }
    va_end(args);
}

void wlog(LogLevel level, const wchar_t* format, ...)
{
    if(!LOG_TO_CONSOLE && !LOG_TO_FILE)   //checking the settings
        return;

    if(LOG_LEVEL < level)
        return;

    va_list args;
    va_start(args, format);

    if(LOG_TO_CONSOLE)
    {
        //console:
        vwprintf(format, args);
    }

    if(LOG_TO_FILE)
    {
        if(0 == strlen(fileNameBuf))    //log file name not initialised yet
        {
            time_t rawtime;
            struct tm * timeinfo;

            time (&rawtime);
            timeinfo = localtime (&rawtime);
            sprintf(&fileNameBuf[0], LOG_FILE_NAME_FORMAT,
                    timeinfo->tm_year + 1900,
                    timeinfo->tm_mon + 1,
                    timeinfo->tm_mday,
                    timeinfo->tm_hour,
                    timeinfo->tm_min,
                    timeinfo->tm_sec);
        }

        //file:
        FILE* fp = fopen(fileNameBuf, "a");
        vfwprintf(fp, format, args);
        fclose(fp);
    }
    va_end(args);
}

//logging helpers
void logFunctionEntered(const char* functionName)
{
    log_(VERBOSE, "-> %s entered\n", functionName);
}

void logFunctionReturnsInt(const char* functionName, int retVal, const char* remark, LogLevel level)
{
    log_(level, "<- %s returned %d. %s\n", functionName, retVal, remark);
}

void logFunctionReturnsVoid(const char* functionName)
{
    log_(VERBOSE, "<- %s returned\n", functionName);
}
