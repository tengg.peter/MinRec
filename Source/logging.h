#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include "constants.h"
#include "enums.h"

#ifndef LOGGING_H
#define LOGGING_H

/*! \file logging.h Functions used for logging to the console and/or file. */

/*!
 * \brief log_ Depending on the values of \see LOG_TO_CONSOLE and \see LOG_TO_FILE, writes logs to either the consol or a log file.
 * If needed a log file is created by the function. It works the same way as the printf functions. You can specify a format string and pass a
 * variable number of parameters with the data to be printed. When LOG_TO_FILE is true, it slows down the program by a great extent, so that
 * settings is only recommended for debugging purposes.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 * \param format A format string that works the same way as in the printf functions.
 */
void log_(LogLevel level, const char* format, ...); //chose log_ name to avoid name collision with reserved identifier

/*!
 * \brief wlog The wide char version of \see log(). Depending on the values of \see LOG_TO_CONSOLE and \see LOG_TO_FILE, writes logs to either the consol or a log file.
 * If needed a log file is created by the function. It works the same way as the printf functions. You can specify a format string and pass a
 * variable number of parameters with the data to be printed. When LOG_TO_FILE is true, it slows down the program by a great extent, so that
 * settings is only recommended for debugging purposes.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 * \param format A format string that works the same way as in the printf functions.
 */
void wlog(LogLevel level, const wchar_t* format, ...);

//logging helpers
/*!
 * \brief logFunctionEntered Helper function to print a function entered message. Its logging level is VERBOSE.
 * \param functionName The name of the function that got entered. It will be printed with the additional standard enter message.
 */
void logFunctionEntered(const char* functionName);

/*!
 * \brief logFunctionReturnsInt Helper function for logging when a function with int return type returns. Its logging level is VERBOSE.
 * \param functionName Name of the function that returns.
 * \param retVal The value being returned by the function.
 * \param remark Additional remark that is added to the message.
 * \param level Verbosity level of printing. This function prints only if the LOG_LEVEL constant in \see constants.h is greater or equal to this
 * parameter.
 */
void logFunctionReturnsInt(const char* functionName, int retVal, const char* remark, LogLevel level);

/*!
 * \brief logFunctionReturnsVoid Helper function for logging when a void function returns. Its logging level is VERBOSE.
 * \param functionName Name of the function that returns.
 */
void logFunctionReturnsVoid(const char* functionName);

#endif // LOGGING_H
