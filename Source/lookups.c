#include "stdafx.h"
#include "lookups.h"
#include <stddef.h>
#include "helper.h"
#include "logging.h"

const int MAX_CIV_ID = 18;

const char* civs[] =
{
	"unknown",   //0
	"Britons",  //1
	"Franks",   //2
	"Goths",     //3
	"Teutons",  //4
	"Japanese", //5
	"Chinese",  //6
	"Byzantines",//7
	"Persians", //8
	"Saracens", //9
	"Turks",    //10
	"Vikings",  //11
	"Mongols",  //12
	"Celts",    //13
	"Spanish",  //14
	"Aztecs",   //15
	"Mayans",   //16
	"Huns",     //17
	"Koreans"  //18
};

const int CUSTOM_MAP_ID = 44;

static const char* ARABIA = "Arabia";   //9
static const char* ARCHIPELAGO = "Archipelago";  //10
static const char* BALTIC = "Baltic";   //11
static const char* BLACK_FOREST = "Black Forest";   //12
static const char* COASTAL = "Coastal";   //13
static const char* CONTINENTAL = "Continental";   //14
static const char* CRATER_LAKE = "Crater Lake";   //15
static const char* FORTRESS = "Fortress";   //16
static const char* GOLD_RUSH = "Gold Rush";   //17
static const char* HIGHLAND = "Highland";   //18
static const char* ISLANDS = "Islands";   //19
static const char* MEDITERRANEAN = "Mediterranean";   //20
static const char* MIGRATION = "Migration";   //21
static const char* RIVERS = "Rivers";   //22
static const char* TEAM_ISLANDS = "Team Islands";   //23
static const char* SCANDINAVIA = "Scandinavia";   //25
static const char* MONGOLIA = "Mongolia";   //26
static const char* YUCATAN = "Yucatan";   //27
static const char* SALT_MARSH = "Salt Marsh";   //28
static const char* ARENA = "Arena";   //29
static const char* OASIS = "Oasis";   //31
static const char* GHOST_LAKE = "Ghost Lake";   //32
static const char* NOMAD = "Nomad";   //33

static const char* CUSTOM_MAP = "Custom Map";    //44
static const char* BLIND_RANDOM = "Blind Random";   //48
static const char* UNKNOWN_MAP = "unknown map";   //48

const char* getMapNameById(int mapId)
{
	switch (mapId)
	{
	case 9:
		return ARABIA;
	case 10:
		return ARCHIPELAGO;
	case 11:
		return BALTIC;
	case 12:
		return BLACK_FOREST;
	case 13:
		return COASTAL;
	case 14:
		return CONTINENTAL;
	case 15:
		return CRATER_LAKE;
	case 16:
		return FORTRESS;
	case 17:
		return GOLD_RUSH;
	case 18:
		return HIGHLAND;
	case 19:
		return ISLANDS;
	case 20:
		return MEDITERRANEAN;
	case 21:
		return MIGRATION;
	case 22:
		return RIVERS;
	case 23:
		return TEAM_ISLANDS;
	case 25:
		return SCANDINAVIA;
	case 26:
		return MONGOLIA;
	case 27:
		return YUCATAN;
	case 28:
		return SALT_MARSH;
	case 29:
		return ARENA;
	case 31:
		return OASIS;
	case 32:
		return GHOST_LAKE;
	case 33:
		return NOMAD;
	case 44:
		return CUSTOM_MAP;
	case 48:
		return BLIND_RANDOM;
	default:
		return UNKNOWN_MAP;
	}
}


int isAcceptedExtension(const wchar_t* extension)
{
	if (NULL == extension)
		return 0;

	if (0 == wcscmp(extension, L".mgz") ||
		0 == wcscmp(extension, L".mgx"))
		return 1;

	return 0;
}

int isAcceptedVersion(const char* version)
{
	logFunctionEntered("isAcceptedVersion()");
	log_(VERBOSE, "%s\n", version);

	if (NULL == version)
		return 0;

	if (0 == strcmp(version, "VER 9.3") ||
		0 == strcmp(version, "VER 9.4") ||
		0 == strcmp(version, "VER 9.C") ||
		0 == strcmp(version, "VER 9.D") ||
		0 == strcmp(version, "VER 9.E") ||
		0 == strcmp(version, "TRL 9.3"))
	{
		logFunctionReturnsInt("isAcceptedVersion()", 1, "Accepted version.", VERBOSE);
		return 1;
	}

	log_(BASIC, "<- isAcceptedVersion() returns 0: %s is not an accepted version\n", version);
	return 0;
}
