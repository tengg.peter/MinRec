#ifndef LOOKUPS_H
#define LOOKUPS_H

#include <wchar.h>

/*! \file lookups.h Variables and functions that help translating ID numbers to human readable strings like civilisations and maps.*/

/*!
 * \brief MAX_CIV_ID The greatest possible civilisation ID. An ID greater than this means that something is wrong.
 */
extern const int MAX_CIV_ID;
/*!
 * \brief civs A string array that contains the name of each civ. The civ's ID indexes the array.
 */
extern const char* civs[];

/*!
 * \brief CUSTOM_MAP_ID Not an inbuilt map in the game, but scripted. Its ID is 44.
 * If 44 is found in the data, the map name must be guessed from the instructions.
 */
extern const int CUSTOM_MAP_ID;
/*!
 * \brief getMapNameById Returns the user friendly name of a map specified by its ID.
 * \param mapId The ID of the map.
 * \return  A char pointer to the map name.
 */
const char* getMapNameById(int mapId);

/*!
 * \brief isAcceptedExtension Checks if the file extension is a valid recorded game extension. Currently .mgx and .mgz are accepted.
 * \param extension Pointer to the extension.
 * \return
 */
int isAcceptedExtension(const wchar_t* extension);

/*!
 * \brief isAcceptedVersion Checks if a version is an accepted recorded game version. For the accepted versions see \see isAcceptedVersion()
 * in lookups.c
 * \param version Pointer to the version as string.
 * \return
 */
int isAcceptedVersion(const char* version);

#endif // LOOKUPS_H
