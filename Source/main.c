#include "stdafx.h"
#include <windows.h>
#include <tchar.h>
#include "rename.h"
#include "helper.h"
#include "lookups.h"
#include "logging.h"
#include "tests.h"

#define BUFSIZE MAX_PATH

int main()
{
    logFunctionEntered("main()");

    WIN32_FIND_DATA ffd;
    HANDLE hFind;
    DWORD dwError, dwRet;

    TCHAR buffer[BUFSIZE];
    dwRet = GetCurrentDirectory(BUFSIZE, buffer);
    if(0 == dwRet)
    {
        dwError = GetLastError();
        log_(BASIC, "Error getting current directory. Error code: %d\n. Program exits.", dwError);
        return 1;
    }

    int currentDirPathLength = wcslen(buffer);
    wlog(MODERATE, L"Current directory: %ls\n", buffer);

    hFind = FindFirstFile(TEXT(".\\*"), &ffd);

    if(INVALID_HANDLE_VALUE == hFind)
    {
        wlog(BASIC, L"Invalid handle value\n");
        return 1;
    }

    wchar_t newName[1000];
    do
    {
        if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            wlog(MODERATE, L"%ls <DIR>\n", ffd.cFileName);
        }
        else
        {
            wlog(BASIC, L"Original file name: %s\n", ffd.cFileName);
            wchar_t* extension = getExtension(ffd.cFileName);
            if(isAcceptedExtension(extension))    //checks if rec
            {
                if(getNewFileName(&ffd, currentDirPathLength, newName, 1000) == OK)
                {
                    wlog(BASIC, L"\nNew name: %ls\n", newName);

                    _wrename(ffd.cFileName, newName); //renames the last rec

                    wlog(VERBOSE, L"errno: %d\n", errno);
                }
            }
            wlog(BASIC, L"===================================================================\n"
                        L"===================================================================\n");
        }
    }
    while(FindNextFile(hFind, &ffd));

    dwError = GetLastError();
    if(dwError != ERROR_NO_MORE_FILES)
    {
        wlog(VERBOSE, L"FindFirstFile()\n");
    }

    FindClose(hFind);

    if(18 == dwError)
        log_(VERBOSE, "No more files.\n");
    else
        log_(VERBOSE, "Error code: %d\n", dwError);

//    scanf("");

    logFunctionReturnsInt("main()", 0, "", VERBOSE);
    return 0;
}

