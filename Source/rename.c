#include "stdafx.h"
#include "rename.h"
#include <sys/stat.h>
#include "byteconversions.h"
#include "lookups.h"
#include "helper.h"
#include "logging.h"
#include "enums.h"
#include "TeamBuilding/teambuilding_functions.h"
#include "Analyse/analyse_body.h"
#include "Analyse/analyse_header.h"

int getNewFileName(WIN32_FIND_DATA* pWfd, int currentDirPathLength, wchar_t* buf, int bufSize)
{
    logFunctionEntered("getNewFileName()");

    int ret = 0;
    FILE* recFile = _wfopen(pWfd->cFileName, L"rb");

    //determining the file size
    struct stat fileStat;
    int fileDesc = fileno(recFile);
    fstat(fileDesc, &fileStat);
    int fileSize = fileStat.st_size;
    log_(MODERATE, "\nFile size calculated: %d\n", fileSize);

    //reading file content
    byte* fileContent = (byte*)malloc(fileSize * sizeof(byte));
    int read = fread(fileContent, sizeof(byte), fileSize, recFile); //reads the whole file to memory
    if(read != fileSize)
    {
        fclose(recFile);
        free(fileContent);
        logFunctionReturnsInt("getNewFileName()", BAD_DATA, "Unsuccessful reading of file content.", BASIC);
        return BAD_DATA;   //unsuccessful read from rec
    }

    int headerLength = convertBytesToInt(fileContent);
    if(headerLength >= fileSize)    //probably not rec file
    {
        fclose(recFile);
        free(fileContent);
        logFunctionReturnsInt("getNewFileName()", BAD_DATA, "Incorrect header length. Probably not a rec file.", BASIC);
        return BAD_DATA;
    }

    GameData gameData;
    initGameData(&gameData);

    ret = getHeaderData(fileContent + 8, headerLength - 8, &gameData);    //the compressed data starts 8 bytes after the beginning of the file
    if(ret != OK)
    {
        fclose(recFile);
        free(fileContent);
        finaliseGameData(&gameData);
        logFunctionReturnsInt("getNewFileName()", ret, "Problem getting header data.", BASIC);
        return ret;
    }
    analyseBodyData2(&fileContent[headerLength], fileSize - headerLength, &gameData);

    log_(MODERATE, "Game data after analysing the rec file: \n");
    printGameData(&gameData, MODERATE);

    if(gameData.numPlayersWithoutGaia > 8 || gameData.numPlayersWithoutGaia < 1) //something is wrong with the data
    {
        fclose(recFile);
        free(fileContent);
        finaliseGameData(&gameData);
        log_(BASIC, "<- getNewFileName() returns %d. Number of players (%d) is out of range.\n", BAD_DATA, gameData.numPlayersWithoutGaia);
        return BAD_DATA;
    }

    //building up the teams
    Player* teams[MAX_PLAYERS][MAX_PLAYERS];
    initTeamsArray(teams);
    buildTeamsArrayFromDiplomacy(&gameData, teams);
    orderTeams(teams, &gameData);

    //determining the map name
    const char* mapName;
    char mapNameBuf[100];
    if(SCENARIO == gameData.gameType)  //scenario
    {
        mapName = "Scenario";
    }
    else if(CUSTOM_MAP_ID == gameData.mapId)   //custom map
    {
        getMapNameFromInstructions(&gameData, mapNameBuf, 100);
        mapName = mapNameBuf;
    }
    else    //normal map. Resolving its name
    {
        mapName = getMapNameById(gameData.mapId);
    }

    //building the filename
    wchar_t* wExtension = getExtension(pWfd->cFileName);
    char extension[10]; //10 just in case of crazy extensions in the future. or HD aoc
    wcstombs(extension, wExtension, 9); //leaving place for \0

    int winningTeamIndex = getWinningTeamIndex(teams);

    char narrowNewName[1024];
    strcpy(narrowNewName, "Rec - ");

    //iterating over the teams array
    int i = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        if(NULL == teams[i][0])
            continue;

        if(winningTeamIndex != -1)   //-1 means winner couldn't be decided
        {
            if(i == winningTeamIndex)
            {
                strcat(narrowNewName, "[W] ");
            }
            else
            {
                strcat(narrowNewName, "[L] ");
            }
        }

        int j = 0;
        for (j = 0; j < MAX_PLAYERS; j++)
        {
            if(teams[i][j] != NULL)
            {
                if(openingCoopParenthesisNeeded(teams[i], j)) //if this is coop, we have to add the opening parenthesis
                {
                    strcat(narrowNewName, "(");
                }

                strcat(narrowNewName, teams[i][j]->name);
                if(2 == gameData.numPlayersWithoutGaia)   //1v1 game, the civ is added
                {
                    byte civId = teams[i][j]->civ;
                    civId = civId > 18 ? 0 : civId;
                    strcat(narrowNewName, "(");
                    strcat(narrowNewName, civs[civId]);
                    strcat(narrowNewName, ")");
                }
                else if(closingCoopParenthesisNeeded(teams[i], j))
                {
                    strcat(narrowNewName, ")"); //close coop parentheses
                }

                if(j < 7 && teams[i][j+1] != NULL)
                {
                    strcat(narrowNewName, ", ");  //writes "," only if there is a next player in the team
                }
            }
        }   //end for each player in team
        if(i < 7 && teams[i+1][0] != NULL)  //writes "vs" only if there is an opponent team left
        {
            strcat(narrowNewName, " vs ");
        }
    }   //end for each team
    //adding the mapname
    strcat(narrowNewName, " - ");
    strcat(narrowNewName, mapName);
    strcat(narrowNewName, " - ");

    //adding the file creation time
    SYSTEMTIME lastWriteTime;
    char timeBuf[10];
    FileTimeToSystemTime(&pWfd->ftLastWriteTime, &lastWriteTime);

    snprintf(timeBuf, 10, "%d", lastWriteTime.wYear);
    strcat(narrowNewName, timeBuf);   //adding year
    strcat(narrowNewName, "-");

    snprintf(timeBuf, 10, "%02d", lastWriteTime.wMonth);
    strcat(narrowNewName, timeBuf);   //adding month
    strcat(narrowNewName, "-");

    snprintf(timeBuf, 10, "%02d", lastWriteTime.wDay);
    strcat(narrowNewName, timeBuf);   //adding day
    strcat(narrowNewName, " ");

    snprintf(timeBuf, 10, "%02d", lastWriteTime.wHour);
    strcat(narrowNewName, timeBuf);   //adding hour
    strcat(narrowNewName, "'");

    snprintf(timeBuf, 10, "%02d", lastWriteTime.wMinute);
    strcat(narrowNewName, timeBuf);   //adding minute
    strcat(narrowNewName, "'");

    snprintf(timeBuf, 10, "%02d", lastWriteTime.wSecond);
    strcat(narrowNewName, timeBuf);   //adding second

    strcat(narrowNewName, extension);

    mbstowcs(buf, narrowNewName, bufSize);    //Converts multibyte string to wide-character string
    removeInvalidCharacters(buf);
    trimNewName(buf, currentDirPathLength);

    fclose(recFile);
    log_(VERBOSE, "Rec file closed.\n");
    finaliseGameData(&gameData);
    free(fileContent);
    log_(VERBOSE, "File content freed.\n");

    logFunctionReturnsInt("getNewFileName()", OK, "", VERBOSE);
    return OK;
}

int getMapNameFromInstructions(const GameData* pGameData, char* buf, int bufLength)
{
    logFunctionEntered("getMapNameFromInstructions()");

    int colonFound = 0;
    char* pColon = pGameData->instructions;

    do
    {
        pColon = strchr(pColon, ':');
        if(pColon != NULL)
            colonFound++;
        if(colonFound < 3)
            pColon++;   //moves one, so we can continue searching
    }while(colonFound < 3 && pColon != NULL);

    if(NULL == pColon)
    {
        logFunctionReturnsInt("getMapNameFromInstructions()", 1, "Map name not found.", MODERATE);
        return 1;
    }

    char* pNewLine = strchr(pColon, '\n');
    if(NULL == pNewLine)
    {
        logFunctionReturnsInt("getMapNameFromInstructions()", 1, "Map name not found.", MODERATE);
        return 1;
    }

    int mapNameLength = pNewLine - pColon - 2;  //-2 because the space and colon are not needed
    memcpy(buf, pColon+2, bufLength);
    buf[mapNameLength] = '\0';

    logFunctionReturnsInt("getMapNameFromInstructions()", OK, "", VERBOSE);
    return OK;
}

int openingCoopParenthesisNeeded(Player* team[MAX_PLAYERS], int currentPlayerIndex)
{
    logFunctionEntered("openingCoopParenthesisNeeded()");

    int i = currentPlayerIndex; //shorter convenience name

    if(i > 0 && //not the first player
       team[i]->coopIndex != 0 &&   //has a valid coop index
       team[i-1]->coopIndex == team[i]->coopIndex)  //the previous player has the same coop index
    {
        logFunctionReturnsInt("openingCoopParenthesisNeeded()", 0, "The previous player has the same coop index.", VERBOSE);
        return 0;
    }

    if(i < 7 &&    //not the last player in the team
       team[i]->coopIndex != 0 &&   //valid coop index
       team[i+1] != NULL && //following player is not null
       team[i]->coopIndex == team[i+1]->coopIndex)  //same coop index
    {
        logFunctionReturnsInt("openingCoopParenthesisNeeded()", 1, "", VERBOSE);
        return 1;
    }

    logFunctionReturnsInt("openingCoopParenthesisNeeded()", 0, "Any other reason.", VERBOSE);
    return 0;
}

int closingCoopParenthesisNeeded(Player* team[MAX_PLAYERS], int currentPlayerIndex)
{
    logFunctionEntered("closingCoopParenthesisNeeded()");

    int i = currentPlayerIndex;
    if(0 == i) //this is the first player. No need to close the parenthesis
    {
        logFunctionReturnsInt("closingCoopParenthesisNeeded()", 0, "This is the first player. No need to close the parenthesis.", VERBOSE);
        return 0;
    }

    if((7 == i || NULL == team[i+1]) &&    //there is no next player
       team[i]->coopIndex != 0 &&   //has a valid coopindex
       team[i]->coopIndex == team[i-1]->coopIndex)  //the previos player has the same coop index
    {
        logFunctionReturnsInt("closingCoopParenthesisNeeded()", 1, "", VERBOSE);
        return 1;
    }

    if(i > 0 && //not the first player
       team[i]->coopIndex != 0 &&   //has a valid coopindex
       team[i-1]->coopIndex == team[i]->coopIndex && //the previous player has the same coopindex
       team[i]->coopIndex != team[i+1]->coopIndex)  //the next player has a different coopindex, this is the end of the cooping
    {
        logFunctionReturnsInt("closingCoopParenthesisNeeded()", 1, "", VERBOSE);
        return 1;
    }

    logFunctionReturnsInt("closingCoopParenthesisNeeded()", 0, "Any other case.", VERBOSE);
    return 0;
}

int getWinningTeamIndex(Player* teams[MAX_PLAYERS][MAX_PLAYERS])
{
    logFunctionEntered("getWinnerTeamIndex()");

    int winnerTeamIndex = 0, nooneResigned = 1;
    int resignedCount = 0, minResignedCount = MAX_PLAYERS;    //we count the players who resigned, because that is an action. The default value is not resigned

    int i = 0, j = 0;
    for (i = 0; i < MAX_PLAYERS; i++)
    {
        resignedCount = 0;
        for (j = 0; j < MAX_PLAYERS; j++)
        {
            if(NULL == teams[i][j])
            {
                break;
            }
            if(1 == teams[i][j]->resigned)
            {
                resignedCount++;
                nooneResigned = 0;
            }
        }
        if(teams[i][0] && resignedCount < minResignedCount) //teams[i][0]: checks if the team is not empty
        {
            minResignedCount = resignedCount;
            winnerTeamIndex = i;
        }
    }
    if(nooneResigned)   //noone resigned, the winner cannot be decided (probably quit with alt+f4)
    {
        logFunctionReturnsInt("getWinningTeamIndex()", -1, "Noone resigned.", VERBOSE);
        return -1;
    }

    log_(VERBOSE, "-> getWinningTeamIndex() returns. Winning team index is %d\n", winnerTeamIndex);
    return winnerTeamIndex;
}

//the total file name length can only be 260 characters. This function makes sure the new name doesn't exceed that.
void trimNewName(wchar_t* newName, int currentDirPathLength)
{
    logFunctionEntered("trimNewName()");
    int newNameLength = wcslen(newName);
    int totalPathLength = currentDirPathLength + newNameLength;

    if(totalPathLength > MAX_PATH)
    {
        int trimLength = totalPathLength - (MAX_PATH - 1);    //MAX_PATH is 260, but in reality the name can only be 259. The rest is the trailing zero
        wchar_t* pLastDot = wcsrchr(newName, L'.');
        wchar_t extension[10];
        wcscpy(extension, pLastDot);
        wlog(VERBOSE, L"extension: %ls\n", extension);
        int extensionLength = wcslen(extension);
        wcscpy(&newName[newNameLength - trimLength - extensionLength - 4], L"..."); //-4 because three dots and a trailing zero
        wlog(VERBOSE, L"new name: %ls\n", newName);
        wcscpy(&newName[newNameLength - trimLength - extensionLength - 1], extension);  //-1 to make room for the trailing zero
        wlog(VERBOSE, L"new name: %ls\n", newName);
    }
    logFunctionReturnsVoid("trimNewName()");
}

void removeInvalidCharacters(wchar_t* fileName)
{
    logFunctionEntered("removeInvalidCharacters()");

    const wchar_t* invalidChars = L"\\/:*?\"<>|";  // invalid chars: \/:*?"<>|
    wchar_t* pRead = fileName, *pWrite = fileName;

    //the idea comes from here: http://stackoverflow.com/questions/9895216/how-to-remove-all-occurrences-of-a-given-character-from-string-in-c
    while(*pRead)
    {
        *pWrite = *pRead++; //the character is written from read to write then read advances forward
        //write only advances if its value is a valid character. Else the invalid one will be overwritten in the next iteration
        if(!wcschr(invalidChars, *pWrite))
            pWrite++;
    }
    *pWrite = L'\0';
    logFunctionReturnsVoid("removeInvalidCharacters()");
}
