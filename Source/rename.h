#ifndef RENAME_H
#define RENAME_H

#include <wchar.h>
#include "GameDataStruct/gamedata.h"

/*! \file rename.h Functions used to put the new file name together. */

/*!
 * \brief getNewFileName This is the highest level renaming function. It gets a pointer to a rec file and returns the new file name for it.
 * \param pWfd Pointer to the file being renamed.
 * \param currentDirPathLength The number of characters in the path to the current directory. It is important to know because it can limit
 * the length of the new file name. The total length has to be less than 260
 * \param buf Buffer for the new filename. The caller has to make sure that it is large enough and will be freed.
 * \param bufSize The size of the buffer.
 * \return OK if the new file name was generated successfully or an error code.
 */
int getNewFileName(WIN32_FIND_DATA* pWfd, int currentDirPathLength, wchar_t* buf, int bufSize);

/*!
 * \brief getMapNameFromInstructions Tries to extract the map name from the instructions. It is useful when the game was played on a custom map.
 * \param pGameData Pointer to a \see GameData struct that contains the instructions.
 * \param buf Buffer for the map name. The caller has the responsibility to allocate and free it.
 * \param bufLength Length of the buffer.
 * \return OK if the map name could be etracted, 1 otherwise.
 */
int getMapNameFromInstructions(const GameData* pGameData, char* buf, int bufLength);

/*!
 * \brief openingCoopParenthesisNeeded Determines if an opening coop parenthesis is needed in the file name. The function returns 1 if the current
 * player is the first in a coop, and is followed at least by one cooping player.
 * \param team The team whose players are written to the file. The team should be already ordered.
 * \param currentPlayerIndex The index of the player whose name is going to be written next.
 * \return 1 if an opening coop parenthesis is needed, 0 otherwise.
 */
int openingCoopParenthesisNeeded(Player* team[MAX_PLAYERS], int currentPlayerIndex);

/*!
 * \brief closingCoopParenthesisNeeded Determines if a closing coop parenthesis is needed in the file name. The function returns 1 if the current
 * player is the last in a coop, and is preceeded at least by one cooping player.
 * \param team The team whose players are written to the file. The team should be already ordered.
 * \param currentPlayerIndex The index of the player whose name is going to be written next.
 * \return 1 if a closing coop parenthesis is needed, 0 otherwise.
 */
int closingCoopParenthesisNeeded(Player* team[MAX_PLAYERS], int currentPlayerIndex);

/*!
 * \brief getWinningTeamIndex Returns the index of the winning team. There can be 8 teams at most and the index can be between 0 and 7
 * \param teams Pointer to a 8x8 teams array.
 * \return The winner team's index or -1 if it couldn't be determined.
 */
int getWinningTeamIndex(Player* teams[MAX_PLAYERS][MAX_PLAYERS]);

/*!
 * \brief trimNewName Checks if the length of the new name plus the path to the curren location exceeds 260, the MAX_PATH in Windows. If it does,
 * it makes the new name shorter to fit the restriction.
 * \param newName Pointer to the new name.
 * \param currentDirPathLength The number of characters in the current path.
 */
void trimNewName(wchar_t* newName, int currentDirPathLength);

/*!
 * \brief removeInvalidCharacters Removes characters from the new name that are forbidden in Windows. Invalid chars: \/:*?"<>|
 * \param fileName Pointer to the file name from which the characters will be removed.
 */
void removeInvalidCharacters(wchar_t* fileName);

#endif // RENAME_H
