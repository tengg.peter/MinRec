#include "stdafx.h"
#include "tests.h"
#include "TeamBuilding/teambuilding_functions.h"
#include "helper.h"

void testInitTeamsArray()
{
    Player* array[MAX_PLAYERS][MAX_PLAYERS];

    initTeamsArray(array);

    printTeamsArray(array, BASIC);

    Player p;
    p.team = 11;
    array[5][5] = &p;

    printTeamsArray(array, BASIC);
}
