#ifndef TESTS_H
#define TESTS_H

#include "GameDataStruct/gamedata.h"

/*! \file tests.h Some functions for quick tests. */

/*!
 * \brief testInitTeamsArray I used this function to test if passing the 8x8 teams array to a function actually works or if I had to pass it by
 * pointer. The syntax of passing an array works without introducing another level of indirection (a pointer).
 */
void testInitTeamsArray();

#endif // TESTS_H
