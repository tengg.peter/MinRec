#include "stdafx.h"
#include "zlibfunctions.h"
#include "Zlib\zlib.h"
#include "helper.h"

#define CHUNK 262144 //256 KB

int zlibDecompress(byte* pSource, int sourceSize, byte** ppDest, int* pDestSize)
{
    int ret, leftFromSource = sourceSize, inSize;
    unsigned produced, destAllocated;
    z_stream strm;
    byte in[CHUNK];
    byte out[CHUNK];
    byte* sourcePtr = pSource;

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit2(&strm, -MAX_WBITS);
    if (ret != Z_OK)
        return ret;

    *ppDest = (byte*)malloc(sizeof(byte) * 5 * sourceSize);
    if(NULL == *ppDest)
    {
        return Z_MEM_ERROR;
    }
    byte* destPtr = *ppDest;
    destAllocated = 5 * sourceSize;
    *pDestSize = 0;

    /* decompress until deflate stream ends or end of file */
    do {
        inSize = leftFromSource > CHUNK ? CHUNK : leftFromSource;
        leftFromSource -= inSize;
        memcpy(in, sourcePtr, inSize);
        strm.avail_in = inSize;
        if (0 == strm.avail_in)
            break;
        strm.next_in = in;
        sourcePtr += inSize;
        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                return ret;
            }
            produced = CHUNK - strm.avail_out;

            if(*pDestSize + produced > destAllocated)   //not enough room in the destination buffer. Allocates more memory
            {
                *ppDest = realloc(*ppDest, destAllocated + CHUNK);
                if(NULL == *ppDest)
                {
                    inflateEnd(&strm);
                    return Z_MEM_ERROR;
                }
                destAllocated += CHUNK;
                destPtr = *ppDest + *pDestSize; //the destPtr has to be updated, since the memory block may have been moved by realloc
            }
            memcpy(destPtr, out, produced);
            *pDestSize += produced;
            destPtr += produced;
        } while (0 == strm.avail_out);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return Z_STREAM_END == ret ? Z_OK : Z_DATA_ERROR;
}
