#ifndef ZLIBFUNCTIONS
#define ZLIBFUNCTIONS

#include <stdio.h>
#include <windows.h>
#include <assert.h>

/*! \file zlibfunctions.h Code related to decompressing the compressed data. */

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

/*!
 * \brief zlibDecompress Decompresses the compressed data using zlib.
 * \param pSource Pointer to the byte array that holds the compressed data.
 * \param sourceSize Length of the compressed data in bytes.
 * \param ppDest Pointer to a pointer to which zlibDecompress will allocate memory to store the decompressed data. The calles has to free it.
 * \param pDestSize Pointer to an int that will contain the size of the decompressed data after the function has run.
 * \return Z_OK if the decompression was successful, an errorcode otherwise.
 */
int zlibDecompress(byte* pSource, int sourceSize, byte** ppDest, int* pDestSize);

#endif // ZLIBFUNCTIONS

